package com.test;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import com.exceptions.*;

public class RegisterTest {
	
	@Test
	public void userNameAlreadyExistsTest() {
		Exception e = assertThrows(AlreadyExistingUserException.class, () ->
			Validator.checkUserAlreadyExists("tejuwuu")
		);
		
		String expectedMessage = "User name already exists! Please select a different one.";
		String actualMessage = e.getMessage();
		assertTrue(expectedMessage.contains(actualMessage));
	}
	
	@Test
	public void passwordsDoNotMatchTest()
	{
		Exception e = assertThrows(PasswordsDoNotMatchException.class, () ->
		Validator.checkPasswordMatch("123abc", "123ab")
		);
	
		String expectedMessage = "Passwords do not match! Please check again.";
		String actualMessage = e.getMessage();
		assertTrue(expectedMessage.contains(actualMessage));
	}
	
	@Test
	public void InsufficientDepositTest()
	{
		Exception e = assertThrows(InitialDepositCheckException.class, () ->
		Validator.checkInitialDepositAmount("savings", 900)
		);
	
		String expectedMessage = "Minimum balance for savings account is 1000/-";
		String actualMessage = e.getMessage();
		assertTrue(expectedMessage.contains(actualMessage));
	}
	
	@Test
	public void ContactNumberInvalidTest()
	{
		Exception e = assertThrows(ContactNoException.class, () ->
		Validator.checkContactNo("723642813")
		);
	
		String expectedMessage = "Contact number cannot be less than 10 digits!";
		String actualMessage = e.getMessage();
		assertTrue(expectedMessage.contains(actualMessage));
	}
}

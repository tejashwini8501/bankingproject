package com.exceptions;

public class PasswordsDoNotMatchException extends Exception{
	
	public PasswordsDoNotMatchException(String msg)
	{
		super(msg);
	}

}

package com.exceptions;

public class AlreadyExistingUserException extends Exception{
	
	public AlreadyExistingUserException(String msg)
	{
		super(msg);
	}

}

package com.exceptions;

public class InitialDepositCheckException extends Exception{
	
	public InitialDepositCheckException(String msg)
	{
		super(msg);
	}

}

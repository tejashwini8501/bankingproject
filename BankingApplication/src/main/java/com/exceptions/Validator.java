package com.exceptions;

import java.sql.SQLException;

import com.servlets.UserDao;

public class Validator {
	
	public static void checkUserAlreadyExists(String userName) throws AlreadyExistingUserException, SQLException
	{
		if(UserDao.userAlreadyExists(userName))
		{
			throw new AlreadyExistingUserException("User name already exists! Please select a different one.");
		}
	}
	
	public static void checkPasswordMatch(String pass, String RePass) throws PasswordsDoNotMatchException {
		if(!pass.equals(RePass)) 
		{
			throw new PasswordsDoNotMatchException("Passwords do not match! Please check again.");
		}
	}
	
	public static void checkInitialDepositAmount(String accType, double balance) throws InitialDepositCheckException{
		if(accType.equalsIgnoreCase("savings") && balance < 1000.0)
		{
			throw new InitialDepositCheckException("Minimum balance for savings account is 1000/-");
		}
		if(accType.equalsIgnoreCase("current") && balance < 2000)
		{
			throw new InitialDepositCheckException("Minimum balance for current account is 2000/-");
		}
	}
	
	public static void checkContactNo(String phone) throws ContactNoException{
		if(phone.length()!=10)
		{
			throw new ContactNoException("Contact number cannot be less than 10 digits!");
		}
	}
	

}

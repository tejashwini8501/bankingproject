package com.servlets;

import java.io.IOException;
//import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/RejectPaymentRequest")
public class RejectPaymentRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try
		{
			response.setContentType("text/html");
			
			RequestDispatcher dispatcher;
			
			HttpSession session = request.getSession();
			
			String userName = (String) session.getAttribute("userName");
			
			if(userName != null)
			{
				Connection con = Util.getConnection();
				Statement stmt = con.createStatement();
				
				String sql = "select * from user where user_name = '" + userName + "';";
				ResultSet rs = stmt.executeQuery(sql);
				
				if(rs.next())
				{
					String paymentStatus = request.getParameter("status");
		 			int reqID = Integer.parseInt(request.getParameter("reqID"));

					String sql3 = "select * from payment_requests where user_name = '"+userName+"' and Request_id = "+reqID+"";
					ResultSet rs3 = stmt.executeQuery(sql3);
					
					if(rs3.next())
					{
						if(paymentStatus.equalsIgnoreCase("Paid"))
		 				{
		 					request.setAttribute("msg","AlreadyPaid");
		 					dispatcher=request.getRequestDispatcher("paymentRequests.jsp");
		 					dispatcher.include(request, response);
		 				}
						else if(paymentStatus.equalsIgnoreCase("Rejected"))
		 				{
		 					request.setAttribute("msg","AlreadyRejected");
		 					dispatcher=request.getRequestDispatcher("paymentRequests.jsp");
		 					dispatcher.include(request, response);
		 				}
						else
						{
							String sql5 = "update payment_requests set payment_status = 'Rejected' where user_name = '"+userName+"' and Request_id = "+reqID+" ";
							int x = stmt.executeUpdate(sql5);
							
							if(x!=0)
							{
								request.setAttribute("msg","Rejected");
			 					dispatcher=request.getRequestDispatcher("paymentRequests.jsp");
			 					dispatcher.include(request, response);
							}
						}
						
					}
				}
				
				con.close();
				stmt.close();
			}
			else
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

package com.servlets;

import java.io.IOException;
//import java.io.PrintWriter;
import java.sql.ResultSet;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/AccountStatusCheck")
public class AccountStatusCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			response.setContentType("text/html");
			
			RequestDispatcher dispatcher;
	
			HttpSession session = request.getSession();
			int id = Integer.parseInt(request.getParameter("id"));
			String userName = (String) session.getAttribute("userName");

			if(userName != null)
			{				
				String sql = "select * from services where  delete_user = 1 and user_name = '"+userName+"';";
				ResultSet rs = Util.getConnection().createStatement().executeQuery(sql);
				
				if(rs.next())
				{
					if(rs.getString("delete_status").equalsIgnoreCase("Pending"))
					{
						request.setAttribute("msg","AppliedForDelAcc");
						dispatcher=request.getRequestDispatcher("home1.jsp");
						dispatcher.forward(request, response);
					}
				
				}
			
				else
				{
					if(id==1) 
					{
						dispatcher=request.getRequestDispatcher("withdraw.html");
						dispatcher.forward(request, response);
					}
					else if(id==2) 
					{
						dispatcher=request.getRequestDispatcher("deposit.html");
						dispatcher.forward(request, response);
					}
					else if(id==3) 
					{
						dispatcher=request.getRequestDispatcher("transferDetails.jsp");
						dispatcher.forward(request, response);
					}
					else if(id == 4)
					{
						dispatcher=request.getRequestDispatcher("busTicketSearch.jsp");
						dispatcher.forward(request, response);
					}
					else if(id == 5)
					{
						dispatcher=request.getRequestDispatcher("viewBookings.jsp");
						dispatcher.forward(request, response);
					}
				}
				
			}
			else
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

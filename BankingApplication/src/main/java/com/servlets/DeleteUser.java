package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/DeleteUser")
public class DeleteUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			
			RequestDispatcher dispatcher;
	
			Connection con = Util.getConnection();
			Statement stmt = con.createStatement();
			
			int accNo = Integer.parseInt(request.getParameter("accNo"));
			
			String sql = "select * from services where acc_no = '"+accNo+"' and delete_user = 1";
			ResultSet rs = stmt.executeQuery(sql);
			
			if(rs.next())
			{
		
				if(rs.getString("delete_status").equalsIgnoreCase("Pending"))
				{
			
					String sql1="delete from user where acc_no= '"+accNo+"' ";
					
					int x = stmt.executeUpdate(sql1);
					if(x!=0)
					{
						String sql2 = "update services set `delete_status` = 'Deleted' where acc_no = '"+accNo+"' and delete_user = 1 ";
						stmt.executeUpdate(sql2);
						
				  		request.setAttribute("msg","UserDeleted");
						dispatcher=request.getRequestDispatcher("adminServiceRequests.jsp");
						dispatcher.forward(request, response);
					}
				}
				else
				{

					request.setAttribute("msg","AlreadyDeleted");
					dispatcher=request.getRequestDispatcher("adminServiceRequests.jsp");
					dispatcher.forward(request, response);
					
				}
					
				
			}
			else
			{
				RequestDispatcher rd = request.getRequestDispatcher("adminServiceRequests.jsp");
				rd.include(request, response);
				out.println("<center><font color = 'red'; weight = bolder;>No request to delete.</font></center>");
			}
			
			con.close();
			stmt.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

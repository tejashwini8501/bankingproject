package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/BusTicketPaymentRequest")
public class BusTicketPaymentRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		try
	 	{
			response.setContentType("text/html");
			
			RequestDispatcher dispatcher;
			
			PrintWriter out = response.getWriter();
			HttpSession session = request.getSession();
			
			String userName = (String) session.getAttribute("userName");
			
			String username = request.getParameter("username");

			if(userName != null)
			{
			
				if(username.equals(userName))
				{
					Connection con = Util.getConnection();
					Statement stmt = con.createStatement();
					
					String sql = "select * from user where user_name = '" + userName + "';";
					ResultSet rs = stmt.executeQuery(sql);
					
					if(rs.next())
					{
						int acc_no = rs.getInt("acc_no");
						
						String paymentMode = request.getParameter("payment-mode");
						
						String user_full_name = (String) session.getAttribute("full-name");
						int user_agee = (Integer) session.getAttribute("user-age");
						String from_pickup = (String) session.getAttribute("pickUp");
						String to_dest = (String) session.getAttribute("drop");
						String bus_name = (String) session.getAttribute("busName");
						int bus_id = (Integer) session.getAttribute("busID");	
						String travel_date = (String) session.getAttribute("travelDate");
						String travel_time = (String) session.getAttribute("travelTime");
						double amount = (double) session.getAttribute("ticketPrice");
						
						DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  	
						LocalDateTime now = LocalDateTime.now();
						String appliedTime = dtf.format(now);
	
						Random rand = new Random();
						int seat_no = rand.nextInt((25 - 1) + 1) + 1;					
						int ticket_no = rand.nextInt((200-1) + 1) + 1;
					
						String sql1 = "insert into bus_tickets (acc_no, full_name, age, from_pickup, to_dest, bus_name, bus_id, seat_no, ticket_no, travel_date, "
								+ "travel_time, ticket_price, booking_status, dateandtime) values("+acc_no+",'"+user_full_name+"', "+user_agee+", '"+from_pickup
								+"', '"+to_dest+"', '"+bus_name+"', "+bus_id+", "+seat_no+", "+ticket_no+", '"+travel_date+"', '"+travel_time+"', "+amount
								+", 'Applied', '"+appliedTime+"')";
						int y = stmt.executeUpdate(sql1);
						
						if(y!=0)
						{
							
							String sql2 = "select booking_id from bus_tickets where acc_no = "+acc_no+" and dateandtime = '"+appliedTime+"'";
							ResultSet rs1 = stmt.executeQuery(sql2);
					
							if(rs1.next())
							{
								int booking_ID =  rs1.getInt("booking_id");

								sql2 = "insert into `payment_requests` (`payment_mode`, `amount`, `dateandtime`, `user_name`, `payment_status`, `type_of_service`, `to_acc_no`, `booking_id`)  values('"+paymentMode+"', "+amount+", '"+LocalDateTime.now()+"' , '"+userName+"', 'Yet to pay', 'Bus Ticket', 0, "+booking_ID+") ";
					       		int x = stmt.executeUpdate(sql2);
					       		
					       		if(x!=0)
					       		{	
					       			request.setAttribute("msg","PaymentRequested");
									dispatcher=request.getRequestDispatcher("paymentRequests.jsp");
									dispatcher.forward(request, response);
					       
					       		}
							}
						}
				}
					
		       		con.close();
			       	stmt.close();
				}
				else
				{
					RequestDispatcher rd = request.getRequestDispatcher("busTicketPaymentRequest.jsp");
					rd.include(request, response);
					out.println("<br><center><font color = 'red'; weight = bolder;>You entered your user name incorrectly!</font></center>");
				}
			}
			else
			{	
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
				
			}
			
	 	}
	 	catch(Exception e)
	 	{
	 		e.printStackTrace();
	 	}
	}


}

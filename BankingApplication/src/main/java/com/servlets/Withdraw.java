package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/Withdraw")
public class Withdraw extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			response.setContentType("text/html");
			
			PrintWriter out = response.getWriter();

			RequestDispatcher dispatcher;
			
			HttpSession session = request.getSession();
			
			String userName = (String) session.getAttribute("userName");
			
			if(userName != null)
			{
				double amt = Double.parseDouble(request.getParameter("amount"));
				
				Connection con = Util.getConnection();
				Statement stmt = con.createStatement();
				
				String sql = "select * from user where  user_name = '" + userName + "';";
				ResultSet rs = stmt.executeQuery(sql);

				if(rs.next()) 
				{
					int accNo = rs.getInt("acc_no");
					double bal = rs.getDouble("balance");
					String type = rs.getString("acc_type");
					
			       	if((type.equalsIgnoreCase("savings") && (bal - amt >= 1000)) 
			       			|| (type.equalsIgnoreCase("current") && (bal - amt >= 2000))) 
			       	{
			       		sql = "update user set balance = " + (bal-amt) + " where acc_no = " + accNo;
			       		stmt.executeUpdate(sql);

			       		String sql1 = "insert into transactions(from_acc_no, to_acc_no, amount, dateandtime, acc_no, transaction_type, mode) values("+accNo+", 0, "+amt+", '"+LocalDateTime.now()+"', "+accNo+", 'Withdraw', 'Debited')";
						stmt.executeUpdate(sql1);
						
			       		request.setAttribute("msg","Withdrawn");
						dispatcher=request.getRequestDispatcher("transactions.jsp");
						dispatcher.forward(request, response);
		        	}
			       	else 
			       	{
			       	
						RequestDispatcher rd = request.getRequestDispatcher("withdraw.html");
						rd.include(request, response);
						out.println("<br><center><font color = 'red'; weight = bolder;>Balance goes below minimum! Cannot withdraw!</font></center>");
			       	}
				}
				
				else
				{
					RequestDispatcher rd = request.getRequestDispatcher("withdraw.html");
					rd.include(request, response);
					out.println("<br><center><font color = 'red'; weight = bolder;>Account not found!!</font></center>");
				}
				
				con.close();
				stmt.close();
			}
			else
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}

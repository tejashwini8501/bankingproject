package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/AdminLogin")
public class AdminLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			
			response.setContentType("text/html");
			
			PrintWriter out = response.getWriter();
			
			String adminUser = request.getParameter("adminuser");
			
			Connection con = Util.getConnection();
			Statement stmt = con.createStatement();
			String sql = "select * from admin where admin_id = '" + adminUser+"' ";
			ResultSet rs = stmt.executeQuery(sql);
	
	
			if(rs.next()) {
				
				HttpSession session = request.getSession();
			
				session.setAttribute("adminUser", adminUser);

				RequestDispatcher rd = request.getRequestDispatcher("adminhome.jsp"); 
				rd.forward(request, response);
			
			}
			
			else {
			
				RequestDispatcher rd = request.getRequestDispatcher("adminlogin.jsp");
				rd.include(request, response);
				out.println("<br><center><font color = 'red'; weight = bolder;>Invalid Credentials! Please login again</font></center>");
			}
	
			con.close();
			stmt.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

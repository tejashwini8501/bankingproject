package com.servlets;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/BookingStatusCheck")
public class BookingStatusCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			response.setContentType("text/html");

			RequestDispatcher dispatcher;
	
			HttpSession session = request.getSession();
			
			String userName = (String) session.getAttribute("userName");

			if(userName != null)
			{				
				int bookingID = Integer.parseInt(request.getParameter("bookingID"));
				session.setAttribute("bookingid", bookingID);
				
				String sql = "select booking_status from bus_tickets where acc_no = (select acc_no from user where user_name = '" + userName + "') and booking_id = "+bookingID+";";
				ResultSet rs = Util.getConnection().createStatement().executeQuery(sql);
				
				if(rs.next())
				{
					String cancel_stat = rs.getString("booking_status");
					if(cancel_stat.equalsIgnoreCase("Applied for cancellation"))
					{
						request.setAttribute("msg","AlreadyAppliedForBusCancel");
						dispatcher=request.getRequestDispatcher("viewBookings.jsp");
						dispatcher.forward(request, response);
					}
					else if(cancel_stat.equalsIgnoreCase("Cancelled"))
					{
						request.setAttribute("msg","AlreadyCancelled");
						dispatcher=request.getRequestDispatcher("viewBookings.jsp");
						dispatcher.forward(request, response);
					}
					else if(cancel_stat.equalsIgnoreCase("Booked"))
					{
						dispatcher=request.getRequestDispatcher("cancelTicket.jsp");
						dispatcher.forward(request, response);
					}
					else if(cancel_stat.equalsIgnoreCase("Applied"))
					{
						dispatcher=request.getRequestDispatcher("cancelTicket.jsp");
						dispatcher.forward(request, response);
					}
				
				}
			}
			else
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

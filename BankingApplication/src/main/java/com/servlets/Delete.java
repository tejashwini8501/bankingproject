package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/Delete")
public class Delete extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		RequestDispatcher dispatcher;
		
		HttpSession session = request.getSession();
		
		String userName = (String) session.getAttribute("userName");
		String username = request.getParameter("username");

		if(userName != null)
		{
		
			if(username.equals(userName))
			{
				Connection con = Util.getConnection();
				Statement stmt = con.createStatement();
				
				String sql = "select acc_no from user where user_name = '" + userName + "';";
				ResultSet rs = stmt.executeQuery(sql);
			
				if(rs.next())
				{
					int acc_no = rs.getInt("acc_no");
				
					String sql1 = "insert into `services` (`acc_no`, `user_name`, `delete_user`, `delete_status`)  values('"+acc_no+"', '"+userName+"', '"+1+"', 'Pending') ";
				
		       		int x = stmt.executeUpdate(sql1);
		       		if(x>0)
		       		{		
		       			request.setAttribute("msg","Requested");
						dispatcher=request.getRequestDispatcher("profilemenu.jsp");
						dispatcher.forward(request, response);
		       
		       		}
				}
	       		con.close();
		       	stmt.close();
		}
			else {
				RequestDispatcher rd = request.getRequestDispatcher("delete.jsp");
				rd.include(request, response);
				out.println("<br><center><font color = 'red'; weight = bolder;>You entered your account number incorrectly!</font></center>");
	   		}
		
		}
		else
		{	
			request.setAttribute("msg","NotLoggedin");
			dispatcher=request.getRequestDispatcher("login1.jsp");
			dispatcher.forward(request, response);
			
		}
		
		
		}
	
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

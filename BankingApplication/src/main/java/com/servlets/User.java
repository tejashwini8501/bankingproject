package com.servlets;

public class User {
	
	String userName;
	String password;
	String rePassword;
	int accNo;
	String accType;
	double amount;
	String address;
	long contact;
	String email;
	String fName;
	
	public User() {
		super();
	}
	
	public User(String userName, String password, String rePassword, String accType, double amount, String address,
			long contact, String email, String fName) {
		super();
		this.userName = userName;
		this.password = password;
		this.rePassword = rePassword;
		this.accType = accType;
		this.amount = amount;
		this.address = address;
		this.contact = contact;
		this.email = email;
		this.fName = fName;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getRePassword() {
		return rePassword;
	}
	public void setRePassword(String rePassword) {
		this.rePassword = rePassword;
	}
	
	public int getAccNo() {
		return accNo;
	}
	public void setAccNo(int accNo) {
		this.accNo = accNo;
	}
	
	public String getAccType() {
		return accType;
	}
	public void setAccType(String accType) {
		this.accType = accType;
	}
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public long getContact() {
		return contact;
	}
	public void setPhone(long contact) {
		this.contact = contact;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}
	
}

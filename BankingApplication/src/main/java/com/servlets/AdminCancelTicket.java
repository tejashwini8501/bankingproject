package com.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/AdminCancelTicket")
public class AdminCancelTicket extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			response.setContentType("text/html");
	
			RequestDispatcher dispatcher;
			
			Connection con = Util.getConnection();
			Statement stmt = con.createStatement();
			
			int accNo = Integer.parseInt(request.getParameter("accNo"));
			
			int bookingID = Integer.parseInt(request.getParameter("bookingID"));
			
			String sql = "select cancel_ticket_status from services where acc_no = '"+accNo+"' and booking_id = "+bookingID+"";
			ResultSet rs = stmt.executeQuery(sql);
			
			if(rs.next())
			{
				String status = rs.getString("cancel_ticket_status");
				if(status.equalsIgnoreCase("Pending"))
				{
					String sql4 = "select * from user where acc_no = '" + accNo + "';";
					ResultSet rs4 = stmt.executeQuery(sql4);
						
					if(rs4.next())
					{
						double bal = rs4.getDouble("balance");
						
						String sql5 = "select ticket_price from bus_tickets where acc_no = "+accNo+" and booking_id = "+bookingID+"";
						ResultSet rs5 = stmt.executeQuery(sql5);
						
						if(rs5.next())
						{
							double amt = rs5.getDouble("ticket_price");
							
							sql = "update user set balance = " + (bal+amt) + " where acc_no = " + accNo +"";
				       		int x = stmt.executeUpdate(sql);
				       								
							if(x!=0)
							{
								String sql1 = "insert into transactions(from_acc_no, to_acc_no,  amount, dateandtime, acc_no, transaction_type, mode) values(0, "+accNo+", "+amt+", '"+LocalDateTime.now()+"', "+accNo+", 'Refund', 'Credited')";
								stmt.executeUpdate(sql1);
						
								String sql2 = "update services set `cancel_ticket_status` = 'Cancelled' where acc_no = '"+accNo+"' and booking_id = "+bookingID+"";
								stmt.executeUpdate(sql2);
								
								String sql3 = "update bus_tickets set `booking_status` = 'Cancelled' where acc_no = '"+accNo+"' and booking_id = "+bookingID+"";
								stmt.executeUpdate(sql3);
								
								request.setAttribute("msg","TicketCancelled");
								dispatcher=request.getRequestDispatcher("adminCancelTicketRequests.jsp");
								dispatcher.forward(request, response);
							}
							
						}
					}		
					
				}
				else
				{

					request.setAttribute("msg","AlreadyCancelled");
					dispatcher=request.getRequestDispatcher("adminCancelTicketRequests.jsp");
					dispatcher.forward(request, response);
					
				}
			}
			
			con.close();
			stmt.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

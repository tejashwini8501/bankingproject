package com.servlets;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
public class UserDao {
	
	public static int saveUser(User user) {
		Statement stmt = null;
		String sql = null;
		try {
			sql = "insert into user(user_name, password, acc_type, balance, address, contact, email, full_name)"
					+ " values('" + user.getUserName()
					+ "', '" + user.getPassword()
					+ "', '" + user.getAccType()
					+ "', " + user.getAmount()
					+ ", '" + user.getAddress()
					+ "', " + user.getContact()
					+ ", '" + user.getEmail() 
					+ "', '"+user.getfName()+"');";
			stmt = Util.getConnection().createStatement();
			return stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public static boolean userAlreadyExists(String userName) throws SQLException {
		boolean value = false;
		Connection con = Util.getConnection();
		Statement stmt = con.createStatement();
		
		String sql = "select * from user where user_name = '" + userName + "';";
		ResultSet rs = stmt.executeQuery(sql);
		if(rs.next())
			value = true;
		
		return value;
	}
}


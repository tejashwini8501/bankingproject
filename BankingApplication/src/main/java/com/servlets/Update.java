package com.servlets;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@WebServlet("/Update")
public class Update extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			response.setContentType("text/html");
			
			RequestDispatcher dispatcher;
			
			HttpSession session = request.getSession();
			
			String userName = (String) session.getAttribute("userName");
			
			if(session.getAttribute("userName") != null)
			{
				String address = request.getParameter("address");
				String phone = request.getParameter("contact");
				long ph = Long.parseLong(phone);
				String email = request.getParameter("email");
				
				Connection con = Util.getConnection();
				Statement stmt = con.createStatement();
				
				String sql = "select * from user where  user_name = '" + userName + "';";
				ResultSet rs = stmt.executeQuery(sql);
				
				if(rs.next())
				{
					int accNo = rs.getInt("acc_no");
					
					sql = "update user set contact = '" + ph + "', email = '" + email + "', address = '" + address + "' where acc_no = '" + accNo +"' ";
		       		stmt.executeUpdate(sql);
		       		
		       		request.setAttribute("msg","Updated");
					dispatcher=request.getRequestDispatcher("profile.jsp");
					dispatcher.forward(request, response);
				}
				else
				{
					RequestDispatcher rd = request.getRequestDispatcher("profile.jsp");
					rd.include(request, response);
				}
				
				con.close();
				stmt.close();
			}
			else
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/Transfer")
public class Transfer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			response.setContentType("text/html");
			
			PrintWriter out = response.getWriter();
			
			RequestDispatcher dispatcher;
			
			HttpSession session = request.getSession();
			
			String userName = (String) session.getAttribute("userName");
			
			if(userName != null)
			{
				int receiver = Integer.parseInt(request.getParameter("receiver"));
				double amt = Double.parseDouble(request.getParameter("amt"));
				
				Connection con = Util.getConnection();
				Statement stmt = con.createStatement();
				
				String sql = "select * from user where  user_name = '" + userName + "';";
				ResultSet rs = stmt.executeQuery(sql);
					
				if(rs.next())
				{
					int accNo = rs.getInt("acc_no");
					
					if(accNo == receiver)
			       	{
			       		RequestDispatcher rd = request.getRequestDispatcher("transfer.html");
						rd.include(request, response);
						out.println("<br><center><font color = 'red'; weight = bolder;>Cannot transfer to self.</font></center>");
			       	}
					
					else 
					{
						double bal = rs.getDouble("balance");
						String type = rs.getString("acc_type");
						
						String sql1 = "select * from user where acc_no = " + receiver;
						ResultSet rs1 = stmt.executeQuery(sql1);
						
						if(rs1.next()) 
						{							
					       	double recBal = rs1.getDouble("balance");
					       	if((type.equalsIgnoreCase("savings") && (bal - amt >= 1000)) 
					       			|| (type.equalsIgnoreCase("current") && (bal - amt >= 2000))) 
					       	{
					       		sql = "update user set balance = " + (bal-amt) + " where acc_no = " + accNo;
					       		sql1 = "update user set balance = " + (recBal+amt) + " where acc_no = " + receiver;
					       		String sql2 = "insert into transactions(from_acc_no, to_acc_no,  amount, dateandtime, acc_no, transaction_type, mode) values("+accNo+", "+receiver+", "+amt+", '"+LocalDateTime.now()+"', "+accNo+", 'Transfer', 'Debited')";
					       		String sql3 = "insert into transactions(from_acc_no, to_acc_no, amount, dateandtime, acc_no, transaction_type, mode) values("+accNo+", "+receiver+",  "+amt+", '"+LocalDateTime.now()+"', "+receiver+", 'Transfer', 'Credited')";
					       	
					       		stmt.executeUpdate(sql);
					       		stmt.executeUpdate(sql1);
					       		stmt.executeUpdate(sql2);
					       		stmt.executeUpdate(sql3);
 		
					       		request.setAttribute("msg","Transferred");
								dispatcher=request.getRequestDispatcher("transactions.jsp");
								dispatcher.forward(request, response);
				        	}					
					       	else 
					       	{
			        			RequestDispatcher rd = request.getRequestDispatcher("transfer.html");
								rd.include(request, response);
								out.println("<br><center><font color = 'red'; weight = bolder;>Balance goes below minimum! Cannot transfer!</font></center>");
				        	}
				       	
						}
						else 
						{
							RequestDispatcher rd = request.getRequestDispatcher("transfer.html");
							rd.include(request, response);
							out.println("<br><center><font color = 'red'; weight = bolder;>Recipient account not found!!</font></center>");
						}
					}
				}
				else 
				{
				
					RequestDispatcher rd = request.getRequestDispatcher("transfer.html");
					rd.include(request, response);
					out.println("<br><center><font color = 'red'; weight = bolder;>Account not found!!</font></center>");
				}
				con.close();
				stmt.close();
			}
			else 
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

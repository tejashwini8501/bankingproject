package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exceptions.Validator;


@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		PrintWriter out = response.getWriter();
		try {
				response.setContentType("text/html");	

				RequestDispatcher dispatcher;
				String fname = request.getParameter("fname");
				String user = request.getParameter("user");
				String pwd = request.getParameter("pwd");
				String rePwd = request.getParameter("re-pwd");
				String accType = request.getParameter("acc-type");
				double balance = Double.parseDouble(request.getParameter("balance"));
				String address = request.getParameter("address");
				String phone = request.getParameter("contact");
				String email = request.getParameter("email");
				
				Validator.checkUserAlreadyExists(user);
				
				Validator.checkPasswordMatch(pwd, rePwd);
				
				Validator.checkInitialDepositAmount(accType, balance);
				
				Validator.checkContactNo(phone);
				long ph = Long.parseLong(phone);
	
				User u = new User(user, pwd, rePwd, accType, balance, address, ph, email, fname);
				UserDao.saveUser(u);
				
				request.setAttribute("msg","Registered");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
				
				
		}
	
		 catch(Exception e)
		{
			RequestDispatcher rd = request.getRequestDispatcher("register1.html");
			rd.include(request, response);
			out.println("<br><br><center><font color = 'red'; weight = bolder;>" + e.getMessage() + "</font></center>");
			e.printStackTrace(); 
			
		}
	}

}


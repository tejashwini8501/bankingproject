package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/TransferPaymentRequest")
public class TransferPaymentRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
	 	{
			response.setContentType("text/html");
			
			RequestDispatcher dispatcher;
			
			PrintWriter out = response.getWriter();
			HttpSession session = request.getSession();
			
			String userName = (String) session.getAttribute("userName");
			
			String username = request.getParameter("username");
			
			String paymentMode = request.getParameter("payment-mode");
			
			double amount = (double) session.getAttribute("transfer_amount");
			int receiverAcc = (Integer) session.getAttribute("receiver");
	
			if(userName != null)
			{
			
				if(username.equals(userName))
				{
					Connection con = Util.getConnection();
					Statement stmt = con.createStatement();
					
					String sql = "insert into `payment_requests` (`payment_mode`, `amount`, `dateandtime`, `user_name`, `payment_status`, `type_of_service`, `to_acc_no`)  values('"+paymentMode+"', "+amount+", '"+LocalDateTime.now()+"' , '"+userName+"', 'Yet to pay', 'Transfer', "+receiverAcc+") ";
				
		       		int x = stmt.executeUpdate(sql);
	
		       		if(x!=0)
		       		{	
		       			request.setAttribute("msg","PaymentRequested");
						dispatcher=request.getRequestDispatcher("paymentRequests.jsp");
						dispatcher.forward(request, response);
		       		}
					
		       		con.close();
			       	stmt.close();
				}
				else
				{
					RequestDispatcher rd = request.getRequestDispatcher("transfer.jsp");
					rd.include(request, response);
					out.println("<br><center><font color = 'red'; weight = bolder;>You entered your user name incorrectly!</font></center>");
				}
			}
			else
			{	
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
				
			}
			
	 	}
	 	catch(Exception e)
	 	{
	 		e.printStackTrace();
	 	}
	}


}

package com.servlets;

import java.io.IOException;
//import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/DeleteReqStatus")
public class DeleteReqStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			response.setContentType("text/html");

			RequestDispatcher dispatcher;

			HttpSession session = request.getSession();
			
			String userName = (String) session.getAttribute("userName");
			
			if(userName != null)
			{
				Connection con = Util.getConnection();
				Statement stmt = con.createStatement();
				
				String sql = "select * from services where delete_user = 1 and user_name = '"+userName+"';";
				ResultSet rs = stmt.executeQuery(sql);
				
				if(rs.next())
				{
					String status = rs.getString("delete_status");
					
					if(status.equalsIgnoreCase("Pending"))
					{
						request.setAttribute("msg","DeleteStatApplied");
						dispatcher=request.getRequestDispatcher("profilemenu.jsp");
						dispatcher.forward(request, response);
					}
				}
				else
				{
					RequestDispatcher rd = request.getRequestDispatcher("delete.jsp");
					rd.forward(request, response);
				}
			}
			else
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

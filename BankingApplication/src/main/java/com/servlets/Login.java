package com.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		try {
			response.setContentType("text/html");

			RequestDispatcher dispatcher;
			String user = request.getParameter("user");
			String pwd = request.getParameter("pwd");
			
			Connection con = Util.getConnection();
			Statement stmt = con.createStatement();
			String sql = "select * from user where user_name = '" + user + "' and password = '" + pwd + "' ";
			ResultSet rs = stmt.executeQuery(sql);

	
			if(rs.next()) 
			{
				int accNo = rs.getInt("acc_no");
				
				HttpSession session = request.getSession();
				session.setAttribute("accNo", accNo);
				session.setAttribute("userName", user);

				RequestDispatcher rd = request.getRequestDispatcher("home1.jsp"); 
				rd.forward(request, response);
			
			}
			
			else 
			{			
				request.setAttribute("msg","InvalidCred");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.include(request, response);
			}
			con.close();
			stmt.close();
		} 
	catch (Exception e) 
	{
	e.printStackTrace();
	}

	}

}



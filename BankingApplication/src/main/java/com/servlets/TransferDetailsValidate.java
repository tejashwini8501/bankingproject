package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/TransferDetailsValidate")
public class TransferDetailsValidate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			response.setContentType("text/html");
			
			PrintWriter out = response.getWriter();
			
			RequestDispatcher dispatcher;
			
			HttpSession session = request.getSession();
			
			String userName = (String) session.getAttribute("userName");
			System.out.println(userName);
			
			if(userName != null)
			{
				int receiver = Integer.parseInt(request.getParameter("receiver"));
				double amt = Double.parseDouble(request.getParameter("amt"));
				
				session.setAttribute("transfer_amount", amt);
				session.setAttribute("receiver", receiver);
				
				Connection con = Util.getConnection();
				Statement stmt = con.createStatement();
				
				String sql = "select * from user where  user_name = '" + userName + "';";
				ResultSet rs = stmt.executeQuery(sql);
					
				if(rs.next())
				{
					int accNo = rs.getInt("acc_no");
					
					if(accNo == receiver)
			       	{
			       		RequestDispatcher rd = request.getRequestDispatcher("transferDetails.jsp");
						rd.include(request, response);
						out.println("<br><center><font color = 'red'; weight = bolder;>Cannot transfer to self.</font></center>");
			       	}
					
					else 
					{
						double bal = rs.getDouble("balance");
						String type = rs.getString("acc_type");
						
						String sql1 = "select * from user where acc_no = " + receiver;
						ResultSet rs1 = stmt.executeQuery(sql1);
						
						if(rs1.next()) 
						{		
							double recBalance = rs1.getDouble("balance");
							session.setAttribute("recBal", recBalance);
							
					       	if((type.equalsIgnoreCase("savings") && (bal - amt >= 1000)) 
					       			|| (type.equalsIgnoreCase("current") && (bal - amt >= 2000))) 
					       	{
					       			RequestDispatcher rd = request.getRequestDispatcher("transfer.jsp");
									rd.forward(request, response);
				        	}					
					       	else 
					       	{
			        			RequestDispatcher rd = request.getRequestDispatcher("transferDetails.jsp");
								rd.include(request, response);
								out.println("<br><center><font color = 'red'; weight = bolder;>Balance goes below minimum! Cannot transfer!</font></center>");
				        	}
				       	
						}
						else 
						{
							RequestDispatcher rd = request.getRequestDispatcher("transferDetails.jsp");
							rd.include(request, response);
							out.println("<br><center><font color = 'red'; weight = bolder;>Recipient account not found!!</font></center>");
						}
					}
				}
				con.close();
				stmt.close();
			}
			else 
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

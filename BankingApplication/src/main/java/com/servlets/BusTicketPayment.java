package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/BusTicketPayment")
public class BusTicketPayment extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			
			response.setContentType("text/html");
			
			PrintWriter out = response.getWriter();
			
			RequestDispatcher dispatcher;
			
			HttpSession session = request.getSession();
			
			String userName = (String) session.getAttribute("userName");
			
			String username = request.getParameter("username1");

			if(userName != null)
			{	
				if(username.equals(userName))
				{
					Connection con = Util.getConnection();
					Statement stmt = con.createStatement();
					
					String sql = "select * from user where user_name = '" + userName + "';";
					ResultSet rs = stmt.executeQuery(sql);
					
					if(rs.next())
					{

						int accNo = rs.getInt("acc_no");
						double bal = rs.getDouble("balance");
						String type = rs.getString("acc_type");

						int reqID = (Integer) session.getAttribute("reqid");
					
						String sql3 = "select * from payment_requests where user_name = '"+userName+"' and Request_id = "+reqID+"";
						ResultSet rs3 = stmt.executeQuery(sql3);
						
						if(rs3.next())
						{
							String typeOfService = rs3.getString("type_of_Service");
							String payMode = rs3.getString("payment_mode");
							double amount = rs3.getDouble("amount");
							
							if(typeOfService.equalsIgnoreCase("Transfer"))
							{
								int receiverAccno = rs3.getInt("to_acc_no");
								
								String sql6 = "select balance from user where acc_no = "+receiverAccno+"";
								ResultSet rs6 = stmt.executeQuery(sql6);
								
								if(rs6.next())
								{
									double recBal = rs6.getDouble("balance");
									
									if((type.equalsIgnoreCase("savings") && (bal - amount >= 1000)) 
							       			|| (type.equalsIgnoreCase("current") && (bal - amount >= 2000))) 
							       	{
										sql = "update user set balance = " + (bal-amount) + " where acc_no = " + accNo;
							       		stmt.executeUpdate(sql);
							       		
							       		String sql1 = "update user set balance = " + (recBal+amount) + " where acc_no = " + receiverAccno;
							       		stmt.executeUpdate(sql1);
										
										String sql2 = "insert into transactions(from_acc_no, to_acc_no,  amount, dateandtime, acc_no, transaction_type, mode) values("+accNo+", "+receiverAccno+", "+amount+", '"+LocalDateTime.now()+"', "+accNo+", '"+payMode+"', 'Debited')";
							       		String sql4 = "insert into transactions(from_acc_no, to_acc_no, amount, dateandtime, acc_no, transaction_type, mode) values("+accNo+", "+receiverAccno+",  "+amount+", '"+LocalDateTime.now()+"', "+receiverAccno+", '"+payMode+"', 'Credited')";
							       		
							       		stmt.executeUpdate(sql2);
							       		stmt.executeUpdate(sql4);
							       		
							       		String sql5 = "update payment_requests set payment_status = 'Paid' where user_name = '"+userName+"' and Request_id = "+reqID+" ";
										stmt.executeUpdate(sql5);
										
										request.setAttribute("msg","Transferred");
										dispatcher=request.getRequestDispatcher("transactions.jsp");
										dispatcher.forward(request, response);
							       	}
									else
									{
										RequestDispatcher rd = request.getRequestDispatcher("paymentOfRequest.jsp");
										rd.include(request, response);
										out.println("<br><center><font color = 'red'; weight = bolder;>Balance goes below minimum! Cannot make payment!</font></center>");
									}
								}
							}
							
							else if(typeOfService.equalsIgnoreCase("Bus Ticket"))
							{
								int bookingID = rs3.getInt("booking_id");
								
								String sql5 = "select * from bus_tickets where acc_no = "+accNo+" and booking_id = "+bookingID+"";
								ResultSet rs6 = stmt.executeQuery(sql5);
								
								if(rs6.next())
								{
	
									if((type.equalsIgnoreCase("savings") && (bal - amount >= 1000)) 
							       			|| (type.equalsIgnoreCase("current") && (bal - amount >= 2000))) 
									{
										sql = "update user set balance = " + (bal-amount) + " where acc_no = " + accNo;
							       		stmt.executeUpdate(sql);
							       		
							       		String sql2 = "insert into transactions(from_acc_no, to_acc_no, amount, dateandtime, acc_no, transaction_type, mode) values("+accNo+", 0, "+amount+", '"+LocalDateTime.now()+"', "+accNo+", '"+payMode+"', 'Debited')";
										stmt.executeUpdate(sql2);

										String sql1 = "update bus_tickets set booking_status = 'Booked' where acc_no = "+accNo+" and booking_id = "+bookingID+" ";
										stmt.executeUpdate(sql1);
										
										String sql4 = "update payment_requests set payment_status = 'Paid' where user_name = '"+userName+"' and Request_id = "+reqID+" ";
										stmt.executeUpdate(sql4);
										
							       		request.setAttribute("msg","MadePayment");
										dispatcher=request.getRequestDispatcher("viewBookings.jsp");
										dispatcher.forward(request, response);
									}
									else
									{
										RequestDispatcher rd = request.getRequestDispatcher("paymentOfRequest.jsp");
										rd.include(request, response);
										out.println("<br><center><font color = 'red'; weight = bolder;>Balance goes below minimum! Cannot make payment!</font></center>");
									}
								}
							}
						}
						
					
					}
					else
					{
						RequestDispatcher rd = request.getRequestDispatcher("paymentOfRequest.jsp");
						rd.include(request, response);
						out.println("<br><center><font color = 'red'; weight = bolder;>Account not found!!</font></center>");
					}
					con.close();
					stmt.close();
				}
				else
				{
					RequestDispatcher rd = request.getRequestDispatcher("paymentOfRequest.jsp");
					rd.include(request, response);
					out.println("<br><center><font color = 'red'; weight = bolder;>You entered your username incorrectly. Please check!</font></center>");
				}
			}
			else
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
    <link rel="stylesheet" type="text/css" href="style1.css" />
  <style>

  
  </style>
  
</head>
<%
	session = request.getSession();
%>
<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
         <li><a href="home1.jsp">Home</a></li>
          <li><a href="profilemenu.jsp">Profile</a></li>
          <li ><a href="banking.jsp">Banking</a></li>
           <li><a href="transactions.jsp">Transactions</a></li>
          <li class="selected"><a href="services.html">Travel</a></li>
        	 <li><a href="paymentRequests.jsp">Payments</a></li>
          <li><a href="Logout">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
   <div id="content_header"></div>
    <div id="site_content">
  		<br><br><br><br><br>
   		
   		 <h1> Confirm Booking</h1>
   		 
   		 <div class="confirmBooking-form">
    <form action="busTicketPaymentRequest.jsp">
         <div class="form_settings">
         
      <p><span>Full Name : </span><input type="text" name="fullName"  required="required" placeholder="Enter your full name" /></p><br>
      <p><span>Age : </span><input type="number" name="age"  required="required" placeholder="Enter your age" /></p><br>  
      <p><span>Select Payment method : </span><select name="payment-method" required><option value="">Select payment method</option><option value="UPI">UPI</option><option value="card">Card</option><option value="account">Account</option></select></p><br>
      <p>Amount to be paid :&emsp;&emsp;&emsp;&emsp;&emsp;<%= request.getParameter("ticketPrice") %></p><br>
 	
	 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<button type="submit" class="submit">Continue
             </button>
             <br><br>
	
	</div>
</form>

	</div>
	<br>
	
	<div class="form_settings">
	
    <button style="float: right;" class="submit" type="button" onclick="history.back()">Back</button>
    
    </div>
        
       </div>
 <%
 	int bus_id = Integer.parseInt(request.getParameter("busID"));
	String bus_name = request.getParameter("busName");
	String fromPickUp = request.getParameter("pickUp");
	String toDest = request.getParameter("drop");
	double ticketPrice = Double.parseDouble(request.getParameter("ticketPrice"));
	String travelDate = request.getParameter("travelDate");
	String travel_time = request.getParameter("travelTime");
	
	session.setAttribute("busID", bus_id);
	session.setAttribute("busName", bus_name);
	session.setAttribute("pickUp", fromPickUp);
	session.setAttribute("drop", toDest);
	session.setAttribute("ticketPrice", ticketPrice);
	session.setAttribute("travelDate", travelDate);
	session.setAttribute("travelTime", travel_time);
 
 
 
 %>

</body>
</html>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.servlets.Util"%>
<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="adminhome.jsp">Users</a></li>
           <li class="selected"><a href="adminTransMenu.jsp">Transactions</a></li>
           <li><a href="adminRequestsMenu.jsp">Requests</a></li>
           <li><a href="adminBusTicketService.jsp">Services</a></li>
          <li><a href="Logout">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  
   <div id="content_header"></div>
    <div id="site_content">
    
		
		<br><br><br><br><br>
   	
        <h1> User Transactions </h1>
        
       <%
     
     try {
			response.setContentType("text/html");
					
			RequestDispatcher dispatcher;
			
			session = request.getSession();
			
			String adminUserName = (String) session.getAttribute("adminUser");
			
			if(adminUserName != null)
			{
		
				Connection con = Util.getConnection();
				Statement stmt = con.createStatement();
				
				String acc_no = request.getParameter("transAccNo");
				int user_account_no = Integer.parseInt(acc_no);
				
				if(user_account_no != 0)
				{
					String sql = "select * from transactions where acc_no = '"+ user_account_no +"' ";
					ResultSet rs = stmt.executeQuery(sql);
				
				%>
				
							<table>
							
				<%
						if(!rs.next()) {
				%>
								<tr>
									<td colspan=4 align="center">User has not made any transactions yet!</td>
								</tr>
				<%
						}
						else
						{
				%>
							<tr>
									<th>Transaction ID</th>
									<th>From</th>
									<th>To</th>
									<th>Amount</th>
									<th>Type of transaction</th>
									<th>Date and Time</th>
							</tr>
								
							<tr>
								<td><%= rs.getInt("transaction_id") %></td>
								<td><%= rs.getInt("from_acc_no") %></td>
								<td><%= rs.getInt("to_acc_no") %></td>
								<td><%= rs.getDouble("amount") %></td>
								<td><%= rs.getString("transaction_type") %></td>
								<td><%= rs.getString("dateandtime") %></td>
							</tr>
								
				<%
					
							
							while(rs.next()) {
							
				%>
								
								<tr>
									<td><%= rs.getInt("transaction_id") %></td>
									<td><%= rs.getInt("from_acc_no") %></td>
									<td><%= rs.getInt("to_acc_no") %></td>
									<td><%= rs.getDouble("amount") %></td>
									<td><%= rs.getString("transaction_type") %></td>
									<td><%= rs.getString("dateandtime") %></td>
								</tr>
				<%
							}
						}
			} 
	       else
	       {
	    	   RequestDispatcher rd = request.getRequestDispatcher("adminTransMenu.jsp");
	  			rd.include(request, response);
	  			out.println("<br><center><font color = 'red'; weight = bolder;>Enter the account number!</font></center>");
	    	   
	       }
			con.close();
			stmt.close();
		}
			else{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("adminlogin.jsp");
				dispatcher.forward(request, response);
			}
     }
       catch (Exception e) {
		e.printStackTrace();
	}
	%>
     </table>
     
     <br>

	<div class="form_settings">
	
    <button style="float: right;" class="submit" type="button" onclick="history.back()">Back</button>
    
    </div>
    
    </div>
</body>
</html>
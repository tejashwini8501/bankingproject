<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.servlets.Util"%>
<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
   <link rel="stylesheet" href="alert/dist/sweetalert.css">
</head>

<body>
<input type="hidden" id="msg" value="<%=request.getAttribute("msg")%>">
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
         <li><a href="home1.jsp">Home</a></li>
          <li><a href="profilemenu.jsp">Profile</a></li>
          <li><a href="banking.jsp">Banking</a></li>
           <li><a href="transactions.jsp">Transactions</a></li>
          <li class="selected"><a href="services.html">Travel</a></li>
         <li><a href="paymentRequests.jsp">Payments</a></li>
          <li><a href="Logout">Logout</a></li>
          
        </ul>
      </div>
    </div>
  </div>
   <div id="content_header"></div>
    <div id="site_content">
  		<br><br><br><br><br>
   	
        <h1> Your bookings </h1>
       
        <%
	try {
		response.setContentType("text/html");
		
		RequestDispatcher dispatcher;
		
		session = request.getSession();
		String userName = (String) session.getAttribute("userName");
		
		if(userName != null) 
		{
			
			Connection con = Util.getConnection();
			Statement stmt = con.createStatement();
			
			String sql = "select * from bus_tickets where acc_no = (select acc_no from user where user_name = '"+userName+"') and booking_status != 'Applied';";
			ResultSet rs = stmt.executeQuery(sql);
			
%>
			<table>
			
			<%
			
			if(!rs.next()) {
			
			%>
				<tr>
						<td colspan=4 align="center">You have not made any bookings yet!</td>
				</tr>
				
			<%
			}
			
			else
			{
			
			%>
				<tr>
					<th>Full Name</th>
					<th>Age</th>
					<th>Bus ID</th>
					<th>Bus Name</th>
					<th>From</th>
					<th>To</th>
					<th>Cost</th>
					<th>Seat No</th>
					<th>Ticket No</th>
					<th>Date</th>
					<th>Time</th>	
					<th>Status</th>
					<th>Cancellation</th>
				</tr>	
				
				<tr>
					<td><%= rs.getString("full_name") %></td>
					<td><%= rs.getInt("age") %></td>
					<td><%= rs.getInt("bus_id") %></td>
					<td><%= rs.getString("bus_name") %></td>
					<td><%= rs.getString("from_pickup") %></td>
					<td><%= rs.getString("to_dest") %></td>
					<td><%= rs.getDouble("ticket_price") %></td>
					<td><%= rs.getInt("seat_no") %></td>
					<td><%= rs.getInt("ticket_no") %></td>
					<td><%= rs.getString("travel_date") %></td>
					<td><%= rs.getString("travel_time") %></td>
					<td><%= rs.getString("booking_status") %></td>
					<td class="tdlink" ><a href='BookingStatusCheck?bookingID=<%= rs.getInt("booking_id") %>'> Cancel </a></td>
				</tr>
		<%
				while(rs.next()) {
		%>
							
				<tr>
					<td><%= rs.getString("full_name") %></td>
					<td><%= rs.getInt("age") %></td>
					<td><%= rs.getInt("bus_id") %></td>
					<td><%= rs.getString("bus_name") %></td>
					<td><%= rs.getString("from_pickup") %></td>
					<td><%= rs.getString("to_dest") %></td>
					<td><%= rs.getDouble("ticket_price") %></td>
					<td><%= rs.getInt("seat_no") %></td>
					<td><%= rs.getInt("ticket_no") %></td>
					<td><%= rs.getString("travel_date") %></td>
					<td><%= rs.getString("travel_time") %></td>
					<td><%= rs.getString("booking_status") %></td>
					<td class="tdlink" ><a href='BookingStatusCheck?bookingID=<%= rs.getInt("booking_id") %>'> Cancel </a></td>
				</tr>
<%
				}
			}
		}
		else 
		{
			request.setAttribute("msg","NotLoggedin");
			dispatcher=request.getRequestDispatcher("login1.jsp");
			dispatcher.forward(request, response);
		}
	} 
   catch (Exception e) 
    {
		e.printStackTrace();
	}
	%>
        	</table>
        	
        	<br>
	
	
	<div class="form_settings">
	
    <a href="services.html"><button style="float: right;" class="submit" type="button">Back</button></a>
    
    </div>
    </div>
    
          
   <script src="vendor/jquery/jquery.min.js"></script>
   <script src="js/main.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
	var status=document.getElementById("msg").value;
	
	if(status=='MadePayment'){
		swal("Congrats", "Ticket booked successfully :)", "success");
		}
	else if(status == 'ReqeuestedCancellation')
		{
			swal("Requested to cancel your ticket.");
		}
	else if(status == 'AlreadyAppliedForBusCancel')
		{
			swal("Oops", "Already applied for cancellation", "error");
		}
	else if(status == 'AlreadyCancelled')
		{
		swal("Oops", "Already cancelled", "error");
		}

	</script>

</body>
</html>
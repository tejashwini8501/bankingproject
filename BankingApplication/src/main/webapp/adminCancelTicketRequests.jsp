<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.servlets.Util"%>
<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
    <link rel="stylesheet" href="alert/dist/sweetalert.css">
</head>

<body>
<input type="hidden" id="msg" value="<%=request.getAttribute("msg")%>">
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="adminhome.jsp">Users</a></li>
           <li><a href="adminTransMenu.jsp">Transactions</a></li>
           <li class="selected"><a href="adminRequestsMenu.jsp">Requests</a></li>
           <li><a href="adminBusTicketService.jsp">Services</a></li>
          <li><a href="Logout">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  
   <div id="content_header"></div>
    <div id="site_content">
    
		
		<br><br><br><br><br>
   	
        <h1> Bus Ticket Cancellation Requests </h1>
        
   <script src="vendor/jquery/jquery.min.js"></script>
   <script src="js/main.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
	var status=document.getElementById("msg").value;

	if(status == 'AlreadyCancelled')
		{
			swal("Oops", "Bus ticket already cancelled", "error");
		}
	else if(status == 'TicketCancelled')
	{
		swal("Yay", "Bus ticket cancelled successfully! Refund is done to the user.", "success");
	}

	</script>
        
       <%
     
     try {
			response.setContentType("text/html");
					
			RequestDispatcher dispatcher;
			
			session = request.getSession();
			
			String adminUserName = (String) session.getAttribute("adminUser");
			
			if(adminUserName != null)
			{
		
					Connection con = Util.getConnection();
					Statement stmt = con.createStatement();
		
					String sql = "select * from services";
					ResultSet rs = stmt.executeQuery(sql);
			%>		
					<table>
					<tr>
							<th>User Name</th>
							<th>Account Number</th>
							<th>Booking ID</th>
							<th>Service Request</th>
							<th>Status</th>
					</tr>
			<%		
					while(rs.next())
					{
						boolean flag = rs.getBoolean("cancel_ticket");
						if(flag)
						{
					
					%>
								<tr>
										<td><%= rs.getString("user_name") %></td>
										<td><%= rs.getInt("acc_no") %></td>
										<td><%= rs.getInt("booking_id") %></td>
										<td class="tdlink" ><a href='AdminCancelTicket?accNo=<%=rs.getInt("acc_no")%>&bookingID=<%= rs.getInt("booking_id")%>'> Cancel Ticket </a></td>
										<td><%= rs.getString("cancel_ticket_status") %></td>
								</tr>
										
						<%
						}
					}

				con.close();
				stmt.close();

			} 
	   
			else
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("adminlogin.jsp");
				dispatcher.forward(request, response);
			}
     }
       catch (Exception e) {
		e.printStackTrace();
	}
	%>
     </table>
     
    
    </div>
</body>
</html>
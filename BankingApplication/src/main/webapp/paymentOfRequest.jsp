<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
    <link rel="stylesheet" type="text/css" href="style1.css" />
  <style>

  
  </style>
  
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
         <li><a href="home1.jsp">Home</a></li>
          <li><a href="profilemenu.jsp">Profile</a></li>
          <li ><a href="banking.jsp">Banking</a></li>
           <li><a href="transactions.jsp">Transactions</a></li>
          <li><a href="services.html">Travel</a></li>
         <li  class="selected"><a href="paymentRequests.jsp">Payments</a></li>
          <li><a href="Logout">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
   <div id="content_header"></div>
    <div id="site_content">
  		<br><br><br><br><br>
	
   		
   		 <%
   		try
 		{
 			response.setContentType("text/html");
 
 			RequestDispatcher dispatcher;
 			
 			session = request.getSession();
 			
 			String userName = (String) session.getAttribute("userName");
 			String paymentStatus = request.getParameter("status");
 			int requestID = Integer.parseInt(request.getParameter("reqID"));
 			
 			session.setAttribute("reqid", requestID); 

 			if(userName != null)
 			{
 				if(paymentStatus.equalsIgnoreCase("Paid"))
 				{
 					request.setAttribute("msg","AlreadyPaid");
 					dispatcher=request.getRequestDispatcher("paymentRequests.jsp");
 					dispatcher.forward(request, response);
 				}
 				else if(paymentStatus.equalsIgnoreCase("Rejected"))
 				{
 					request.setAttribute("msg","AlreadyRejected");
 					dispatcher=request.getRequestDispatcher("paymentRequests.jsp");
 					dispatcher.forward(request, response);
 				}
 				else
 				{
 						
 				%>
 					 <h1> Make your payment</h1>
   		 
 					
 					 <div class="makePayment-form">
       				 <form action="BusTicketPayment">
         			 <div class="form_settings">
         
      				 <span>&emsp;</span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Enter your user name to confirm<br><br> 
      				 <span>&emsp;</span>&emsp;&emsp;&emsp;&emsp;<input type="text" name="username1" required="required" placeholder="Please enter your user name here" required="required" /> <br><br>

	
					 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
	
					 <button style="margin-right:-200px; margin-left:80px;" class="submit" type="submit">Pay</button>
				
					
					 </div>
					
					 </form>
					 </div>
   		
					 <br>
					
					 <div class="form_settings">
					
				     <button style="float: right;" class="submit" type="button" onclick="history.back()">Back</button>
				    
				     </div>
				        
				     </div>
				
 				<%}
 			}
 			else
 			{
 				request.setAttribute("msg","NotLoggedin");
 				dispatcher=request.getRequestDispatcher("login1.jsp");
 				dispatcher.forward(request, response);
 			}
 			
 		}
 		catch(Exception e)
 		{
 			e.printStackTrace();
 		}
 
   		 %>
   
 

</body>
</html>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.servlets.Util"%>
<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
     <link rel="stylesheet" href="alert/dist/sweetalert.css">
</head>

<body>
<input type="hidden" id="msg" value="<%=request.getAttribute("msg")%>">
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
         <li><a href="home1.jsp">Home</a></li>
          <li><a href="profilemenu.jsp">Profile</a></li>
          <li><a href="banking.jsp">Banking</a></li>
           <li class="selected"><a href="transactions.jsp">Transactions</a></li>
          <li><a href="services.html">Travel</a></li>
         <li><a href="paymentRequests.jsp">Payments</a></li>
          <li><a href="Logout">Logout</a></li>
          
        </ul>
      </div>
    </div>
  </div>
   <div id="content_header"></div>
    <div id="site_content">
  		<br><br><br><br><br>
   	
        <h1> Your transactions </h1>
       
        <%
	try {
		response.setContentType("text/html");
		
		RequestDispatcher dispatcher;
		
		session = request.getSession();
		String userName = (String) session.getAttribute("userName");
		
		if(userName != null) 
		{
			Connection con = Util.getConnection();
			Statement stmt = con.createStatement();
			
			String sql = "select * from user where user_name = '" + userName + "';";
			ResultSet rs1 = stmt.executeQuery(sql);
			
			double balance = 0;
			if(rs1.next()) 
			{
				balance = rs1.getDouble("balance");
			}
			
			sql = "select * from transactions where acc_no = (select acc_no from user where user_name = '" + userName + "');";
			ResultSet rs = stmt.executeQuery(sql);
	%>
	
			<table id="transaction_table">
			
	<%
			if(!rs.next()) {
	%>
					<tr>
						<td colspan=4 align="center">You have not made any transactions yet!</td>
					</tr>
					
	<%
			}
			else
			{	
		%>
	
				<tr>
					<th>Transaction ID</th>
					<th>From</th>
					<th>To</th>
					<th>Amount</th>
					<th>Mode</th>
					<th>Date and Time</th>
				</tr>
				
				<tr>
					<td><%= rs.getInt("transaction_id") %></td>
					<td><%= rs.getInt("from_acc_no") %></td>
					<td><%= rs.getInt("to_acc_no") %></td>
					<td><%= rs.getDouble("amount") %></td>
					<td><%= rs.getString("mode") %></td>
					<td><%= rs.getString("dateandtime") %></td>
				</tr>
		<%
				while(rs.next()) {
					//System.out.println("inside trans");
		%>
							
					<tr>
						<td><%= rs.getInt("transaction_id") %></td>
						<td><%= rs.getInt("from_acc_no") %></td>
						<td><%= rs.getInt("to_acc_no") %></td>
						<td><%= rs.getDouble("amount") %></td>
						<td><%= rs.getString("mode") %></td>
						<td><%= rs.getString("dateandtime") %></td>
					</tr>
		<%
					}
			}
			con.close();
			stmt.close();
		}
		else 
		{
			request.setAttribute("msg","NotLoggedin");
			dispatcher=request.getRequestDispatcher("login1.jsp");
			dispatcher.forward(request, response);
		}
	} 
   catch (Exception e) 
    {
		e.printStackTrace();
	}
	%>
        	</table>
        	
        	   <br>
	
	 <div class="form_settings">
	
     <button style="float: right;" class="btn" onclick="ExportToExcel('xlsx')">Download</button>
    
    </div>
    </div>
   
    
    <script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>
	<script type="text/javascript">
	function ExportToExcel(type, fn, dl) {
	    var elt = document.getElementById('transaction_table');
	    var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
	    return dl ?
	    	XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }):
	      	XLSX.writeFile(wb, fn || ('Transactions.' + (type || 'xlsx')));
	}
	</script>
    
   <script src="vendor/jquery/jquery.min.js"></script>
   <script src="js/main.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
	var status=document.getElementById("msg").value;
	
	if(status=='Deposited'){
		swal("Congrats","Amount deposited successfully!","success");
		}
	else if(status=='Transferred')
		{
		swal("Congrats","Amount transferred successfully!","success");
		}
	else if(status=='Withdrawn')
		{
		swal("Congrats","Amount withdrawn successfully!","success");
		}
		
	</script>

</body>
</html>
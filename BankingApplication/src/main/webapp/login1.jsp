<!DOCTYPE HTML>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.servlets.Util"%>
<%@page import="java.sql.Connection"%>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
   <link rel="stylesheet" href="alert/dist/sweetalert.css">
</head>

<body>
<input type="hidden" id="msg" value="<%=request.getAttribute("msg")%>">
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <li><a href="home0.jsp">Home</a></li>
          <li><a href="contact1.html">Contact Us</a></li>
          <li class="selected" ><a href="loginmenu.html">Login</a></li>
          <li><a href="register1.html">Register</a></li>
        </ul>
      </div>
    </div>
  </div>
   <div id="content_header"></div>
    <div id="site_content">
    
    <br><br><br><br>
   	
        <h1> User Login </h1>
        
  	<div class="login-form">
     <form action="Login">
          <div class="form_settings">
          <br><br><br>
            <p><span>User Name : </span><input type="text" name="user"  required="required" placeholder="Enter your username" /></p><br>
            <p><span>Password : </span><input type="password" name="pwd"  required="required" placeholder="Enter your password" /></p><br>
           
             &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<button type="submit" class="submit">Log in
             </button><br><br>
             <span>&nbsp;</span><a href="register1.html" class="register">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;&ensp;New user?</a>
          </div>
     </form>
    </div>
    
    
    <br>
	
	
	<div class="form_settings">
	
    <button style="float: right;" class="submit" type="button" onclick="history.back()">Back</button>
    
    </div>
    </div>
       
	<script src="vendor/jquery/jquery.min.js"></script>
   <script src="js/main.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
	var status=document.getElementById("msg").value;
	
	if(status=='Registered'){
		swal("Congrats", "Registration successful. Please login", "success");
		}
	else if(status=='NotLoggedin')
		{
		swal("Please log in");
		}

	</script>

</body>
</html>
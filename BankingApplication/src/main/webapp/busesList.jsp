<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.servlets.Util"%>
<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
         <li><a href="home1.jsp">Home</a></li>
          <li><a href="profilemenu.jsp">Profile</a></li>
          <li><a href="banking.jsp">Banking</a></li>
           <li><a href="transactions.jsp">Transactions</a></li>
          <li class="selected"><a href="services.html">Travel</a></li>
        	 <li><a href="paymentRequests.jsp">Payments</a></li>
          <li><a href="Logout">Logout</a></li>
          
        </ul>
      </div>
    </div>
  </div>
   <div id="content_header"></div>
    <div id="site_content">
  		<br><br><br><br><br>
   	
        <h1> Buses found </h1>
       
        <%
	try {
		response.setContentType("text/html");
		
		RequestDispatcher dispatcher;
		
		session = request.getSession();
		String userName = (String) session.getAttribute("userName");
		
		String pick_up = request.getParameter("pick-up");
		String dest = request.getParameter("dest");
		String travel_date = request.getParameter("date");
		
		if(userName != null) 
		{
			Connection con = Util.getConnection();
			Statement stmt = con.createStatement();
			
			String sql = "select * from buses where `from_pickup` = '"+pick_up+"' and `to_dest` = '"+dest+"' and `travel_date` = '"+travel_date+"' ";
			ResultSet rs = stmt.executeQuery(sql);
			
			if(!rs.next())
			{
				request.setAttribute("msg","NoBuses");
				dispatcher = request.getRequestDispatcher("busTicketSearch.jsp");
				dispatcher.forward(request, response);
			}
			else
			{
		
		%>
		<table>
				<tr>
					<th>Bus ID</th>
					<th>Bus Name</th>
					<th>From</th>
					<th>To</th>
					<th>Ticket Price</th>
					<th>Date</th>
					<th>Time</th>
					<th>Booking</th>
				</tr>
				
				<tr>
					<td><%= rs.getInt("bus_id") %></td>
					<td><%= rs.getString("bus_name") %></td>
					<td><%= rs.getString("from_pickup") %></td>
					<td><%= rs.getString("to_dest") %></td>
					<td><%= rs.getDouble("ticket_price") %></td>
					<td><%= rs.getString("travel_date") %></td>
					<td><%= rs.getString("travel_time") %></td>
					<td class="tdlink" ><a href='busTicketPayment.jsp?ticketPrice=<%= rs.getDouble("ticket_price") %>&busID=<%= rs.getInt("bus_id") %>&busName=<%= rs.getString("bus_name") %>&pickUp=<%= rs.getString("from_pickup") %>&drop=<%= rs.getString("to_dest") %>&travelDate=<%= rs.getString("travel_date") %>&travelTime=<%= rs.getString("travel_time") %>'> Book </a></td>
				</tr>
<%
				while(rs.next())
				{		
	%>				
					<tr>
						<td><%= rs.getInt("bus_id") %></td>
						<td><%= rs.getString("bus_name") %></td>
						<td><%= rs.getString("from_pickup") %></td>
						<td><%= rs.getString("to_dest") %></td>
						<td><%= rs.getDouble("ticket_price") %></td>
						<td><%= rs.getString("travel_date") %></td>
						<td><%= rs.getString("travel_time") %></td>
						<td class="tdlink" ><a href='busTicketPayment.jsp?ticketPrice=<%= rs.getDouble("ticket_price") %>&busID=<%= rs.getInt("bus_id") %>&busName=<%= rs.getString("bus_name") %>&pickUp=<%= rs.getString("from_pickup") %>&drop=<%= rs.getString("to_dest") %>&travelDate=<%= rs.getString("travel_date") %>&travelTime=<%= rs.getString("travel_time") %>'> Book </a></td>
					</tr>
	<%
				}			
			}
		}
		else 
		{
			request.setAttribute("msg","Not Logged In");
			dispatcher=request.getRequestDispatcher("login1.jsp");
			dispatcher.forward(request, response);
		}
	} 
   catch (Exception e) 
    {
		e.printStackTrace();
	}
	%>
        	</table>
        	
        	<br>
	
	
	<div class="form_settings">
	
    <button style="float: right;" class="submit" type="button" onclick="history.back()">Back</button>
    
    </div>
    </div>

</body>
</html>
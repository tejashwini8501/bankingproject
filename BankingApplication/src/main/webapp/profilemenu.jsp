<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
    <link rel="stylesheet" href="alert/dist/sweetalert.css">
</head>

<body>
<input type="hidden" id="msg" value="<%=request.getAttribute("msg")%>">
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
         <li><a href="home1.jsp">Home</a></li>
          <li class="selected"><a href="profilemenu.jsp">Profile</a></li>
          <li ><a href="banking.jsp">Banking</a></li>
           <li><a href="transactions.jsp">Transactions</a></li>
          <li><a href="services.html">Travel</a></li>
         <li><a href="paymentRequests.jsp">Payments</a></li>
          <li><a href="Logout">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
   <div id="content_header"></div>
    <div id="site_content">
    
  		<br><br><br><br><br><br>
   		<div class="trans-buttons">
   		<form action = "DeleteReqStatus">
   		
  			<a href ="profile.jsp"><button type = "button" >My Profile</button></a><br>
  			<button type = "submit">Delete Account</button><br>
  			
  		</form>
		</div>
        
       </div>
	
	      
   <script src="vendor/jquery/jquery.min.js"></script>
   <script src="js/main.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
	var status=document.getElementById("msg").value;
	
	if(status=='Requested'){
		swal("Request submitted to delete your account.");
		}
	else if(status == 'DeleteStatApplied')
		{
		swal("Your request for deleting account is under process.")
		}

	</script>
	
</body>
</html>
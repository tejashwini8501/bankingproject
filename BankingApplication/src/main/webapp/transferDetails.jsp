<%@page import="com.servlets.Util"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.io.PrintWriter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
         <li><a href="home1.jsp">Home</a></li>
          <li><a href="profilemenu.jsp">Profile</a></li>
          <li class="selected"><a href="banking.jsp">Banking</a></li>
           <li><a href="transactions.jsp">Transactions</a></li>
          <li><a href="services.html">Travel</a></li>
         <li><a href="paymentRequests.jsp">Payments</a></li>
          <li><a href="Logout">Logout</a></li>
          
        </ul>
      </div>
    </div>
  </div>
   <div id="content_header"></div>
    <div id="site_content">
  		<br><br><br><br><br>
   	
        <h1> Enter Details</h1>
        <div class="transferDetails-form">
        <form action="TransferDetailsValidate">
         <div class="form_settings">
         
    <span>&emsp;</span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Enter Recipient's Account Number <br><br> 
	<span>&nbsp;</span>&emsp;&emsp;&emsp;&emsp;<input type="number" name="receiver" required="required" placeholder="Enter the receiver's acc no"><br><br>
	<span>&emsp;</span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;Enter Amount <br><br> 
	<span>&nbsp;</span>&emsp;&emsp;&emsp;&emsp;<input type="number" name="amt" required="required" placeholder="Enter the amount to be transfered"><br><br>
	<span>&emsp;</span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Select Payment method :<br><br> 
	<span>&nbsp;</span>&emsp;&emsp;&emsp;&emsp;<select name="payment-method" required><option value="">Select payment method</option><option value="UPI">UPI</option><option value="card">Card</option><option value="account">Account</option></select><br><br>
	
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<button class="submit" type = "submit" >Continue</button><br><br>
	
	</div>
	</form>
	</div>
        <div class="form_settings">
	
    <button style="float: right;" class="submit" type="button" onclick="history.back()">Back</button>
    
    </div>
       </div>
 
</body>
</html>
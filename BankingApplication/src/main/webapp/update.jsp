<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.servlets.Util"%>
<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
</head>
<body>
	
	<div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
         <li><a href="home1.jsp">Home</a></li>
          <li class="selected"><a href="profilemenu.jsp">Profile</a></li>
          <li ><a href="banking.jsp">Banking</a></li>
           <li><a href="transactions.jsp">Transactions</a></li>
          <li><a href="services.html">Travel</a></li>
         <li><a href="paymentRequests.jsp">Payments</a></li>
          <li><a href="Logout">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
   <div id="content_header"></div>
    <div id="site_content">
    	<br><br><br><br><br>
    	
  		<h1>Update your Profile</h1>
   			
   			<div class="update-form">
   			<form action="Update">
   			<div class="form_settings">
   			
   		 <%
		try
		{
			response.setContentType("text/html");
			
			RequestDispatcher dispatcher;
			
			session = request.getSession();
			
			String userName = (String)session.getAttribute("userName");
			
			if(userName != null)
			{
				Connection con = Util.getConnection();
				Statement stmt = con.createStatement();
				
				String sql = "select * from user where  user_name = '" + userName + "';";
				ResultSet rs = stmt.executeQuery(sql);
				
				if(rs.next())
				{
					
		%>				
					<p><span>Contact Number : </span><input type="tel" name="contact"  required="required" value = <%=rs.getString("contact")%> /></p><br>
		            <p><span>Email ID : </span><input type="email" name="email"  value =  <%=rs.getString("email")%> /></p><br>
		            <p><span>Address : </span><textarea style="resize: none;" rows="3" cols="20" name="address" > <%=rs.getString("address")%></textarea></p><br>
		   			&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<button class="submit"  >Update</button>
					
		<%		
				}
				con.close();
				stmt.close();
			}
			else
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		%>
   			
  
   			</div>
   			</form>
   			</div>
        
		    <div class="form_settings">
			
		    <button style="float: right;" class="submit" type="button" onclick="history.back()">Back</button>
		    
		    </div>
     </div>
</body>
</html>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.servlets.Util"%>
<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
   <link rel="stylesheet" href="alert/dist/sweetalert.css">
</head>

<body>
<input type="hidden" id="msg" value="<%=request.getAttribute("msg")%>">
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
         <li><a href="home1.jsp">Home</a></li>
          <li><a href="profilemenu.jsp">Profile</a></li>
          <li><a href="banking.jsp">Banking</a></li>
           <li><a href="transactions.jsp">Transactions</a></li>
          <li><a href="services.html">Travel</a></li>
         <li class="selected"><a href="paymentRequests.jsp">Payments</a></li>
          <li><a href="Logout">Logout</a></li>
          
        </ul>
      </div>
    </div>
  </div>
   <div id="content_header"></div>
    <div id="site_content">
  		<br><br><br><br><br>
   	
        <h1> Payment Requests </h1>
       
    
     <%
	try {
		response.setContentType("text/html");
		
		RequestDispatcher dispatcher;
		
		session = request.getSession();
		String userName = (String) session.getAttribute("userName");
		
		if(userName != null)
		{
				
			Connection con = Util.getConnection();
			Statement stmt = con.createStatement();

			String sql = "select * from payment_requests where user_name = '"+userName+"';";
			ResultSet rs = stmt.executeQuery(sql);
			
%>
			<table>
			
			<%
			
			if(!rs.next()) {
			
			%>
				<tr>
						<td colspan=4 align="center">You have no payment requests yet!</td>
				</tr>
				
			<%
			}
				
			else
			{
			%>
				<tr>
					<th>Request ID</th>					
					<th>Payment Method</th>
					<th>Amount</th>
					<th>Payment to</th>
					<th>Requested for</th>
					<th>Date and Time</th>
					<th>Payment</th>
					<th>Status</th>
				</tr>	
				
				<tr>
					<td><%= rs.getInt("Request_id") %></td>
					<td><%= rs.getString("payment_mode") %></td>
					<td><%= rs.getDouble("amount") %></td>
					<td><%= rs.getInt("to_acc_no") %></td>
					<td><%= rs.getString("type_of_service") %></td>
					<td><%= rs.getString("dateandtime") %></td>
					<td class="tdlink" ><a href='paymentOfRequest.jsp?status=<%= rs.getString("payment_status") %>&reqID=<%= rs.getInt("Request_id") %>'> Pay now </a><br>
					<a href='RejectPaymentRequest?status=<%= rs.getString("payment_status") %>&reqID=<%= rs.getInt("Request_id") %>'>Reject</a></td>
					<td><%= rs.getString("payment_status") %></td>
				</tr>
		<%
				while(rs.next()) {
		%>
					
				<tr>
					<td><%= rs.getInt("Request_id") %></td>
					<td><%= rs.getString("payment_mode") %></td>
					<td><%= rs.getDouble("amount") %></td>
					<td><%= rs.getInt("to_acc_no") %></td>
					<td><%= rs.getString("type_of_service") %></td>
					<td><%= rs.getString("dateandtime") %></td>
					<td class="tdlink" ><a href='paymentOfRequest.jsp?status=<%= rs.getString("payment_status") %>&reqID=<%= rs.getInt("Request_id") %>'> Pay now </a><br>
					<a href='RejectPaymentRequest?status=<%= rs.getString("payment_status") %>&reqID=<%= rs.getInt("Request_id") %>'>Reject</a></td>
					<td><%= rs.getString("payment_status") %></td>
				</tr>
<%
				}
			}
			con.close();
			stmt.close();
		}
	
		else 
		{
			request.setAttribute("msg","NotLoggedin");
			dispatcher=request.getRequestDispatcher("login1.jsp");
			dispatcher.forward(request, response);
		}
	} 
   catch (Exception e) 
    {
		e.printStackTrace();
	}
	%>
        	</table>
        	
        
    </div>

          
   <script src="vendor/jquery/jquery.min.js"></script>
   <script src="js/main.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
	var status=document.getElementById("msg").value;
	
	if(status=='PaymentRequested'){
		swal("Requested for payment successfully :)");
		}
	else if(status=='AlreadyPaid')
		{
		swal("Oops", "Payment was already done for this request.", "error");
		}
	else if(status == 'Rejected')
		{
		swal("Rejected the payment successfully!")
		}
	else if(status == 'AlreadyRejected')
		{
		swal("Oops", "Payment was already rejected for this request.", "error")
		}

	</script>

</body>
</html>
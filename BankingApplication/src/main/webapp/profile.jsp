<%@page import="java.sql.Connection"%>
<%@page import="com.servlets.Util"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
  <link rel="stylesheet" href="alert/dist/sweetalert.css">
</head>

<body>
<input type="hidden" id="msg" value="<%=request.getAttribute("msg")%>">
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <li><a href="home1.jsp">Home</a></li>
          <li class="selected"><a href="profilemenu.jsp">Profile</a></li>
          <li><a href="banking.jsp">Banking</a></li>
           <li><a href="transactions.jsp">Transactions</a></li>
          <li><a href="services.html">Travel</a></li>
          <li><a href="paymentRequests.jsp">Payments</a></li>
          <li><a href="Logout">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  
   <div id="content_header"></div>
    <div id="site_content">
  		<br><br><br><br><br>
   	
        <h1> Your profile </h1>
        
        <div class="profile-form">
         <div class="profileform_settings">
		
<%
		try
		{
			response.setContentType("text/html");
			
			RequestDispatcher dispatcher;
			
			session = request.getSession();
			
			String userName = (String)session.getAttribute("userName");
			
			if(userName != null)
			{
				Connection con = Util.getConnection();
				Statement stmt = con.createStatement();
				
				String sql = "select * from user where  user_name = '" + userName + "';";
				ResultSet rs = stmt.executeQuery(sql);
				
				if(rs.next())
				{
					
		%>			
					<p><span>Full Name : <%=rs.getString("full_name")%> </span><span> <input style="float: right; background-color: black; color: white;  border: 1px solid; width: 69px; font: 100% arial;  height: 25px;"  type="button" value="Balance" onclick="this.value=' <%=rs.getDouble("balance")%>'"> </span></p>
					<p><span>User Name : <%=rs.getString("user_name")%> </span></p>
					<p><span>Account Number : <%=rs.getInt("acc_no")%> </span></p>
					<p><span>Contact Number : <%=rs.getString("contact")%> </span></p>
					<p><span>Type of Account : <%=rs.getString("acc_type")%> </span></p>
					<p><span>Email ID : <%=rs.getString("email")%> </span></p>
					<p><span>Address : <%=rs.getString("address")%> </span></p>
					<div class = "update-button">
					<a href="update.jsp">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<button class="submit" type="button" name="update">Edit </button></a>
        			</div>
					
		<%		
				}
				
				con.close();
				stmt.close();
			}
			else
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("login1.jsp");
				dispatcher.forward(request, response);
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		%>
	</div>
</div>

<div class="form_settings">
	
    <button style="float: right;" class="submit" type="button" onclick="history.back()">Back</button>
    
    </div>
	
       </div>
       
           <script src="vendor/jquery/jquery.min.js"></script>
   <script src="js/main.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
	var status=document.getElementById("msg").value;
	
	if(status=='Updated'){
		swal("Yay", "Profile updated succesfully!", "success");
		}
	

	</script>
  
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
  <link rel="stylesheet" href="alert/dist/sweetalert.css">
</head>

<body>
<input type="hidden" id="msg" value="<%=request.getAttribute("msg")%>">
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
         <li><a href="home1.jsp">Home</a></li>
          <li><a href="profilemenu.jsp">Profile</a></li>
          <li><a href="banking.jsp">Banking</a></li>
           <li><a href="transactions.jsp">Transactions</a></li>
          <li  class="selected"><a href="services.html">Travel</a></li>
         <li><a href="paymentRequests.jsp">Payments</a></li>
          <li><a href="Logout">Logout</a></li>
          
        </ul>
      </div>
    </div>
  </div>
   <div id="content_header"></div>
    <div id="site_content">
  		<br><br><br><br><br>
   	
        <h1> Search for a bus </h1>
        <div class="busSearch-form">
        <form action="busesList.jsp">
         <div class="form_settings">
         
			<br>
			<p><span>From : </span><select name="pick-up" required><option value="">Select pick-up</option><option value="Bangalore">Bangalore</option><option value="Yadgir">Yadgir</option><option value="Shimoga">Shimoga</option></select></p><br>
            <p><span>To : </span><select name="dest" required><option value="">Select drop</option><option value="Bangalore">Bangalore</option><option value="Yadgir">Yadgir</option><option value="Shimoga">Shimoga</option></select></p><br>
            <p><span>Date : </span><input type="date" name="date"  required="required" /></p><br>
            
             &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<button type="submit" class="submit">Find buses
             </button><br><br>
	
	</div>
	</form>

	</div>
	<br>
	
	
	<div class="form_settings">
	
    <button style="float: right;" class="submit" type="button" onclick="history.back()">Back</button>
    
    </div>

        
       </div>
       
         <script src="vendor/jquery/jquery.min.js"></script>
   <script src="js/main.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
	var status=document.getElementById("msg").value;
	
	if(status=='NoBuses'){
		swal("Sorry", "No buses found :(", "error");
		}

	</script>

</body>
</html>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.servlets.Util"%>
<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="adminhome.jsp">Users</a></li>
           <li  class="selected"><a href="adminTransMenu.jsp">Transactions</a></li>
           <li><a href="adminRequestsMenu.jsp">Requests</a></li>
           <li><a href="adminBusTicketService.jsp">Services</a></li>
          <li><a href="Logout">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  
   <div id="content_header"></div>
    <div id="site_content">
    
		<br><br><br><br><br>
   	
        <h1> User Transactions </h1>
        
		<div class="admintrans-form">
        <form action="adminTransactions.jsp">
         <div class="form_settings">
         
      <span>&emsp;</span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Enter the account number of user<br><br> 
      <span>&nbsp;</span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<input type="number" name="transAccNo" required="required" placeholder="Please enter user's account number"><br><br>
 
	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<button class="submit" type="submit">View transactions</button>
	
	</div>
	
	</form>
	</div>
	
     
       </div>
</body>
</html>
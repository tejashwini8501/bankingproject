<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.servlets.Util"%>
<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li class="selected"><a href="adminhome.jsp">Users</a></li>
           <li><a href="adminTransMenu.jsp">Transactions</a></li>
           <li><a href="adminRequestsMenu.jsp">Requests</a></li>
            <li><a href="adminBusTicketService.jsp">Services</a></li>
          <li><a href="Logout">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  
   <div id="content_header"></div>
    <div id="site_content">
    
		
		<br><br><br><br><br>
   	
        <h1> User Details </h1>
        
       <%
     
     try {
		response.setContentType("text/html");
				
		RequestDispatcher dispatcher;
		
		session = request.getSession();
		
		String adminUserName = (String) session.getAttribute("adminUser");
		
		if(adminUserName != null)
		{
	
			Connection con = Util.getConnection();
			Statement stmt = con.createStatement();
			
			String sql = "select * from user";
			ResultSet rs = stmt.executeQuery(sql);
		
		%>
		
					<table>
					
		<%
				if(!rs.next()) {
		%>
						<tr>
							<td colspan=4 align="center">No current users yet!</td>
						</tr>
		<%
				}
				else
				{
		%>
					<tr>
							<th>Full Name</th>
							<th>User Name</th>
							<th>Account No.</th>
							<th>Account Type</th>
							<th>Contact No.</th>
							<th>Email ID</th>
							<th>Address</th>
							<th>Balance</th>
					</tr>
						
					<tr>
						<td><%= rs.getString("full_name") %></td>
						<td><%= rs.getString("user_name") %></td>
						<td><%= rs.getInt("acc_no") %></td>
						<td><%= rs.getString("acc_type") %></td>
						<td><%= rs.getLong("contact") %></td>
						<td><%= rs.getString("email") %></td>
						<td><%= rs.getString("address") %></td>
						<td><%= rs.getDouble("balance") %></td>
					</tr>
						
		<%
			
					
					while(rs.next()) {
					
		%>
						
						<tr>
							<td><%= rs.getString("full_name") %></td>
							<td><%= rs.getString("user_name") %></td>
							<td><%= rs.getInt("acc_no") %></td>
							<td><%= rs.getString("acc_type") %></td>
							<td><%= rs.getLong("contact") %></td>
							<td><%= rs.getString("email") %></td>
							<td><%= rs.getString("address") %></td>
							<td><%= rs.getDouble("balance") %></td>
						</tr>
		<%
					}
				}
			con.close();
			stmt.close();
		}
		else 
		{
			request.setAttribute("msg","NotLoggedin");
			dispatcher=request.getRequestDispatcher("adminlogin.jsp");
			dispatcher.forward(request, response);
		}
	
	} catch (Exception e) {
		e.printStackTrace();
	}
	%>
     
     	</table>
       </div>
</body>
</html>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.servlets.Util"%>
<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>

<head>
  <title>shadowplay_2</title>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style1.css" />
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index1.html">B<span class="logo_colour">M</span></a></h1>
          <h2>Easy. Secure. Reliable.</h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="adminhome.jsp">Users</a></li>
           <li><a href="adminTransMenu.jsp">Transactions</a></li>
           <li><a href="adminRequestsMenu.jsp">Requests</a></li>
           <li class="selected"><a href="adminBusTicketService.jsp">Services</a></li>
          <li><a href="Logout">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  
   <div id="content_header"></div>
    <div id="site_content">
    
		
		<br><br><br><br><br>
   	
        <h1> Bus bookings </h1>
        
       <%
     
     try {
			response.setContentType("text/html");
					
			RequestDispatcher dispatcher;
			
			session = request.getSession();
			
			String adminUserName = (String) session.getAttribute("adminUser");
			
			if(adminUserName != null)
			{
		
				Connection con = Util.getConnection();
				Statement stmt = con.createStatement();
				
				String sql = "select * from bus_tickets where booking_status != 'Applied'";
				ResultSet rs = stmt.executeQuery(sql);
			
			%>
			
						<table>
						
			<%
					if(!rs.next()) {
			%>
							<tr>
								<td colspan=4 align="center">No bus bookings yet!</td>
							</tr>
			<%
					}
					else
					{
			%>
						<tr>
							<th>Booking ID</th>
							<th>Account Number</th>
							<th>Bus ID</th>
							<th>Bus Name</th>
							<th>From</th>
							<th>To</th>
							<th>Cost</th>
							<th>Date</th>
							<th>Booking status</th>
						</tr>
							
						<tr>
							<td><%= rs.getInt("booking_id") %></td>
							<td><%= rs.getInt("acc_no") %></td>
							<td><%= rs.getInt("bus_id") %></td>
							<td><%= rs.getString("bus_name") %></td>
							<td><%= rs.getString("from_pickup") %></td>
							<td><%= rs.getString("to_dest") %></td>
							<td><%= rs.getDouble("ticket_price") %></td>
							<td><%= rs.getString("travel_date") %></td>
							<td><%= rs.getString("booking_status") %></td>
						</tr>
							
			<%
				
						
						while(rs.next()) {
						
			%>
							
							<tr>
								<td><%= rs.getInt("booking_id") %></td>
								<td><%= rs.getInt("acc_no") %></td>
								<td><%= rs.getInt("bus_id") %></td>
								<td><%= rs.getString("bus_name") %></td>
								<td><%= rs.getString("from_pickup") %></td>
								<td><%= rs.getString("to_dest") %></td>
								<td><%= rs.getDouble("ticket_price") %></td>
								<td><%= rs.getString("travel_date") %></td>
								<td><%= rs.getString("booking_status") %></td>
							</tr>
			<%
						}
					}				
			 
				con.close();
				stmt.close();
			}
			else
			{
				request.setAttribute("msg","NotLoggedin");
				dispatcher=request.getRequestDispatcher("adminlogin.jsp");
				dispatcher.forward(request, response);
			}
     }
       catch (Exception e) {
		e.printStackTrace();
	}
	%>
     </table>
 
    
    </div>
</body>
</html>
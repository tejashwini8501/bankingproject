CREATE DATABASE  IF NOT EXISTS `Banking` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `Banking`;
-- MySQL dump 10.13  Distrib 8.0.29, for Linux (x86_64)
--
-- Host: localhost    Database: Banking
-- ------------------------------------------------------
-- Server version	8.0.30-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `admin_id` varchar(50) DEFAULT NULL,
  `admin_pass` varchar(50) DEFAULT NULL,
  UNIQUE KEY `admin_id` (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('admin1','admin1'),('admin2','admin2'),('admin3','admin3');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bus_tickets`
--

DROP TABLE IF EXISTS `bus_tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bus_tickets` (
  `acc_no` int DEFAULT NULL,
  `full_name` varchar(30) DEFAULT NULL,
  `age` int DEFAULT NULL,
  `from_pickup` varchar(20) DEFAULT NULL,
  `to_dest` varchar(20) DEFAULT NULL,
  `bus_name` varchar(20) DEFAULT NULL,
  `bus_id` int DEFAULT NULL,
  `seat_no` int NOT NULL,
  `ticket_no` int NOT NULL,
  `travel_date` date DEFAULT NULL,
  `travel_time` time DEFAULT NULL,
  `ticket_price` double DEFAULT NULL,
  `booking_status` varchar(45) DEFAULT NULL,
  `booking_id` int NOT NULL AUTO_INCREMENT,
  `dateandtime` varchar(45) DEFAULT NULL,
  UNIQUE KEY `booking_id_UNIQUE` (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bus_tickets`
--

LOCK TABLES `bus_tickets` WRITE;
/*!40000 ALTER TABLE `bus_tickets` DISABLE KEYS */;
INSERT INTO `bus_tickets` VALUES (1,'Tejashwini',21,'Bangalore','Yadgir','VRL',1,16,1,'2022-09-30','21:30:00',1050,'Cancelled',21,NULL),(2,'Vunnati',21,'Bangalore','Shimoga','SVT',3,20,2,'2022-09-30','20:00:00',1500,'Booked',22,NULL),(3,'Sameer',22,'Bangalore','Shimoga','SVT',3,12,3,'2022-09-30','20:00:00',1500,'Cancelled',23,NULL),(10,'Tarun',23,'Bangalore','Yadgir','VRL',1,5,4,'2022-09-30','21:30:00',1050,'Cancelled',24,NULL),(12,'Chiranth',21,'Bangalore','Yadgir','VRL',1,22,8,'2022-09-30','21:30:00',1050,'Cancelled',25,NULL),(13,'Saurav',21,'Bangalore','Yadgir','VRL',1,6,9,'2022-09-30','21:30:00',1050,'Cancelled',26,NULL),(4,'Balaji',22,'Bangalore','Yadgir','VRL',1,16,10,'2022-09-30','21:30:00',1050,'Cancelled',27,NULL),(14,'Dilip',21,'Bangalore','Yadgir','VRL',1,8,11,'2022-09-30','21:30:00',1050,'Booked',28,NULL),(15,'Arijit',22,'Bangalore','Yadgir','VRL',1,18,12,'2022-09-30','21:30:00',1050,'Booked',29,NULL),(16,'Asha',22,'Bangalore','Yadgir','Sanjana',2,1,13,'2022-09-30','22:30:00',1100,'Booked',30,NULL),(17,'Sai Teja',22,'Bangalore','Yadgir','VRL',1,1,63,'2022-09-30','21:30:00',1050,'Cancelled',33,NULL),(17,'Sai Teja',22,'Yadgir','Bangalore','Orange',6,5,162,'2022-10-06','20:30:00',1800,'Cancelled',34,NULL),(17,'Sai Teja',22,'Bangalore','Shimoga','SVT',3,24,25,'2022-09-30','20:00:00',1500,'Cancelled',35,NULL),(17,'Sai Teja',22,'Bangalore','Yadgir','Sanjana',2,3,116,'2022-09-30','22:30:00',1100,'Cancelled',36,NULL),(14,'Dilip',22,'Bangalore','Yadgir','Sanjana',2,11,148,'2022-09-30','22:30:00',1100,'Cancelled',49,'2022-10-10 15:21:10'),(14,'Dilip',22,'Yadgir','Bangalore','Orange',6,10,133,'2022-10-06','20:30:00',1800,'Cancelled',52,'2022-10-10 15:52:59'),(20,'Devesh',22,'Bangalore','Yadgir','Sanjana',2,18,84,'2022-09-30','22:30:00',1100,'Applied',53,'2022-10-10 16:40:01'),(20,'Devesh',22,'Yadgir','Bangalore','Orange',6,2,84,'2022-10-06','20:30:00',1800,'Applied for cancellation',54,'2022-10-10 16:40:24');
/*!40000 ALTER TABLE `bus_tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buses`
--

DROP TABLE IF EXISTS `buses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `buses` (
  `bus_id` int NOT NULL AUTO_INCREMENT,
  `bus_name` varchar(20) DEFAULT NULL,
  `from_pickup` varchar(20) DEFAULT NULL,
  `to_dest` varchar(20) DEFAULT NULL,
  `ticket_price` double DEFAULT NULL,
  `travel_date` date DEFAULT NULL,
  `travel_time` time DEFAULT NULL,
  PRIMARY KEY (`bus_id`),
  UNIQUE KEY `bus_id` (`bus_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buses`
--

LOCK TABLES `buses` WRITE;
/*!40000 ALTER TABLE `buses` DISABLE KEYS */;
INSERT INTO `buses` VALUES (1,'VRL','Bangalore','Yadgir',1050,'2022-09-30','21:30:00'),(2,'Sanjana','Bangalore','Yadgir',1100,'2022-09-30','22:30:00'),(3,'SVT','Bangalore','Shimoga',1500,'2022-09-30','20:00:00'),(4,'SRS','Yadgir','Bangalore',1600,'2022-10-01','20:30:00'),(5,'SVR','Bangalore','Shimoga',1800,'2022-10-01','19:30:00'),(6,'Orange','Yadgir','Bangalore',1800,'2022-10-06','20:30:00');
/*!40000 ALTER TABLE `buses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_requests`
--

DROP TABLE IF EXISTS `payment_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_requests` (
  `Request_id` int NOT NULL AUTO_INCREMENT,
  `payment_mode` varchar(20) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `dateandtime` datetime DEFAULT NULL,
  `user_name` varchar(20) DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `type_of_service` varchar(20) DEFAULT NULL,
  `to_acc_no` int DEFAULT NULL,
  `booking_id` int DEFAULT NULL,
  PRIMARY KEY (`Request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_requests`
--

LOCK TABLES `payment_requests` WRITE;
/*!40000 ALTER TABLE `payment_requests` DISABLE KEYS */;
INSERT INTO `payment_requests` VALUES (1,'UPI',500,'2022-10-07 23:09:43','asha','Paid','Transfer',14,NULL),(13,'UPI',500,'2022-10-08 13:47:23','sai','Paid','Transfer',15,NULL),(15,'account',500,'2022-10-08 13:49:36','sai','Paid','Transfer',10,NULL),(19,'UPI',1050,'2022-10-08 15:29:18','sai','Paid','Bus Ticket',0,17),(20,'account',1800,'2022-10-08 15:29:44','sai','Paid','Bus Ticket',0,18),(21,'account',1500,'2022-10-08 15:42:48','sai','Paid','Bus Ticket',0,19),(22,'card',1100,'2022-10-08 15:44:52','sai','Paid','Bus Ticket',0,20),(23,'UPI',100,'2022-10-09 14:46:42','asha','Rejected','Transfer',15,NULL),(25,'card',1100,'2022-10-10 15:21:10','dilip','Paid','Bus Ticket',0,49),(27,'card',1050,'2022-10-10 15:38:55','dilip','Paid','Bus Ticket',0,51),(28,'card',1800,'2022-10-10 15:53:00','dilip','Paid','Bus Ticket',0,52),(29,'UPI',100,'2022-10-10 16:39:00','dev','Paid','Transfer',10,NULL),(30,'card',1100,'2022-10-10 16:40:01','dev','Rejected','Bus Ticket',0,53),(31,'UPI',1800,'2022-10-10 16:40:25','dev','Paid','Bus Ticket',0,54);
/*!40000 ALTER TABLE `payment_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services` (
  `acc_no` int DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `delete_user` tinyint(1) DEFAULT NULL,
  `delete_status` varchar(50) DEFAULT NULL,
  `cancel_ticket` tinyint(1) DEFAULT NULL,
  `cancel_ticket_status` varchar(50) DEFAULT NULL,
  `booking_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (17,'sai',NULL,NULL,1,'Cancelled',34),(17,'sai',NULL,NULL,1,'Cancelled',33),(17,'sai',NULL,NULL,1,'Cancelled',35),(17,'sai',NULL,NULL,1,'Cancelled',36),(17,'sai',1,'Deleted',NULL,NULL,NULL),(14,'dilip',NULL,NULL,1,'Cancelled',49),(14,'dilip',NULL,NULL,1,'Pending',50),(14,'dilip',NULL,NULL,1,'Cancelled',51),(14,'dilip',NULL,NULL,1,'Pending',51),(14,'dilip',NULL,NULL,1,'Cancelled',52),(20,'dev',NULL,NULL,1,'Pending',54),(20,'dev',1,'Pending',NULL,NULL,NULL);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transactions` (
  `transaction_id` int NOT NULL AUTO_INCREMENT,
  `from_acc_no` int DEFAULT NULL,
  `to_acc_no` int DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `dateandtime` datetime DEFAULT NULL,
  `acc_no` int DEFAULT NULL,
  `transaction_type` varchar(50) DEFAULT NULL,
  `mode` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `transaction_id` (`transaction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (1,0,1,15000,'2022-09-28 16:41:29',1,'Deposit','Credited'),(2,1,0,2500,'2022-09-28 16:41:44',1,'Withdraw','Debited'),(3,1,2,1900,'2022-09-28 16:43:33',1,'Transfer','Debited'),(4,1,2,1900,'2022-09-28 16:43:33',2,'Transfer','Credited'),(5,4,0,500,'2022-09-28 16:47:30',4,'Withdraw','Debited'),(6,0,4,1000,'2022-09-28 16:47:36',4,'Deposit','Credited'),(7,4,1,500,'2022-09-28 16:47:45',4,'Transfer','Debited'),(8,4,1,500,'2022-09-28 16:47:45',1,'Transfer','Credited'),(9,0,2,5000,'2022-09-29 11:41:24',2,'Deposit','Credited'),(10,6,0,5000,'2022-09-29 14:56:42',6,'Withdraw','Debited'),(11,0,6,5000,'2022-09-29 14:56:57',6,'Deposit','Credited'),(12,1,0,50,'2022-09-29 16:18:46',1,'Withdraw','Debited'),(13,1,0,50,'2022-09-29 16:19:31',1,'Withdraw','Debited'),(14,10,0,1050,'2022-09-30 00:23:07',10,'BusBooking','Debited'),(15,0,10,1050,'2022-09-30 00:24:37',10,'Refund','Credited'),(16,0,12,500,'2022-09-30 10:04:26',12,'Deposit','Credited'),(17,12,0,50,'2022-09-30 10:07:54',12,'Withdraw','Debited'),(18,12,0,1050,'2022-09-30 11:07:06',12,'BusBooking','Debited'),(19,12,0,1050,'2022-09-30 11:10:33',12,'BusBooking','Debited'),(20,0,12,1050,'2022-09-30 11:11:49',12,'Refund','Credited'),(21,0,3,1500,'2022-09-30 11:16:16',3,'Refund','Credited'),(22,0,12,3000,'2022-09-30 11:18:34',12,'Deposit','Credited'),(23,12,0,1800,'2022-09-30 11:18:54',12,'BusBooking','Debited'),(24,12,0,1050,'2022-09-30 11:30:07',12,'BusBooking','Debited'),(25,0,12,1050,'2022-09-30 11:31:11',12,'Refund','Credited'),(26,13,0,1050,'2022-09-30 11:54:57',13,'BusBooking','Debited'),(27,0,13,1050,'2022-09-30 11:55:54',13,'Refund','Credited'),(28,4,0,1050,'2022-09-30 16:12:15',4,'card','Debited'),(29,0,4,1050,'2022-09-30 16:15:26',4,'Refund','Credited'),(30,14,0,1050,'2022-09-30 16:28:35',14,'UPI','Debited'),(31,15,0,1050,'2022-09-30 16:30:10',15,'UPI','Debited'),(32,14,15,50,'2022-10-06 12:49:43',14,'UPI','Debited'),(33,14,15,50,'2022-10-06 12:49:43',15,'UPI','Credited'),(34,14,12,50,'2022-10-06 13:32:55',14,'card','Debited'),(35,14,12,50,'2022-10-06 13:32:55',12,'card','Credited'),(46,0,16,3000,'2022-10-06 14:25:27',16,'Deposit','Credited'),(47,16,14,500,'2022-10-07 20:09:23',16,'account','Debited'),(48,16,14,500,'2022-10-07 20:09:23',14,'account','Credited'),(49,16,14,500,'2022-10-07 20:10:12',16,'account','Debited'),(50,16,14,500,'2022-10-07 20:10:12',14,'account','Credited'),(51,16,14,500,'2022-10-07 20:10:59',16,'account','Debited'),(52,16,14,500,'2022-10-07 20:10:59',14,'account','Credited'),(53,16,12,500,'2022-10-07 20:21:01',16,'UPI','Debited'),(54,16,12,500,'2022-10-07 20:21:01',12,'UPI','Credited'),(55,16,12,500,'2022-10-07 20:25:48',16,'UPI','Debited'),(56,16,12,500,'2022-10-07 20:25:48',12,'UPI','Credited'),(57,16,12,500,'2022-10-07 20:29:40',16,'UPI','Debited'),(58,16,12,500,'2022-10-07 20:29:40',12,'UPI','Credited'),(59,16,12,500,'2022-10-07 20:36:06',16,'UPI','Debited'),(60,16,12,500,'2022-10-07 20:36:06',12,'UPI','Credited'),(61,16,14,500,'2022-10-07 20:45:22',16,'account','Debited'),(62,16,14,500,'2022-10-07 20:45:22',14,'account','Credited'),(63,0,16,5000,'2022-10-07 20:45:54',16,'Deposit','Credited'),(64,16,0,1100,'2022-10-07 23:43:07',16,'card','Debited'),(65,16,14,500,'2022-10-07 23:43:34',16,'UPI','Debited'),(66,16,14,500,'2022-10-07 23:43:34',14,'UPI','Credited'),(74,17,10,500,'2022-10-08 13:52:38',17,'account','Debited'),(75,17,10,500,'2022-10-08 13:52:38',10,'account','Credited'),(76,17,0,1800,'2022-10-08 13:54:18',17,'card','Debited'),(77,17,0,1100,'2022-10-08 13:55:07',17,'card','Debited'),(78,17,15,500,'2022-10-08 13:55:58',17,'UPI','Debited'),(79,17,15,500,'2022-10-08 13:55:58',15,'UPI','Credited'),(80,17,0,1800,'2022-10-08 14:56:03',17,'UPI','Debited'),(81,17,0,1100,'2022-10-08 14:56:18',17,'account','Debited'),(82,17,0,1050,'2022-10-08 15:30:20',17,'UPI','Debited'),(83,0,17,5000,'2022-10-08 15:30:38',17,'Deposit','Credited'),(84,17,0,1800,'2022-10-08 15:30:47',17,'account','Debited'),(85,0,17,1050,'2022-10-08 15:34:41',17,'Refund','Credited'),(86,0,17,1800,'2022-10-08 15:37:44',17,'Refund','Credited'),(87,17,0,1500,'2022-10-08 15:42:54',17,'account','Debited'),(88,0,17,1500,'2022-10-08 15:43:15',17,'Refund','Credited'),(89,17,0,1100,'2022-10-08 15:44:57',17,'card','Debited'),(90,0,17,1100,'2022-10-08 15:45:10',17,'Refund','Credited'),(91,14,0,1100,'2022-10-10 15:30:53',14,'card','Debited'),(92,0,14,1100,'2022-10-10 15:32:11',14,'Refund','Credited'),(93,14,0,1050,'2022-10-10 15:39:38',14,'card','Debited'),(94,0,14,1050,'2022-10-10 15:40:56',14,'Refund','Credited'),(95,14,0,1800,'2022-10-10 15:53:17',14,'card','Debited'),(96,0,14,1800,'2022-10-10 15:55:40',14,'Refund','Credited'),(97,20,0,50,'2022-10-10 16:38:33',20,'Withdraw','Debited'),(98,0,20,1000,'2022-10-10 16:38:39',20,'Deposit','Credited'),(99,20,10,100,'2022-10-10 16:39:07',20,'UPI','Debited'),(100,20,10,100,'2022-10-10 16:39:07',10,'UPI','Credited'),(101,20,0,1800,'2022-10-10 16:40:32',20,'UPI','Debited');
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `user_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `acc_no` int NOT NULL AUTO_INCREMENT,
  `acc_type` varchar(50) NOT NULL,
  `balance` double DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `contact` mediumtext,
  `email` varchar(50) DEFAULT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`acc_no`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('tejuwuu','tej',1,'savings',16000,'Yadgir','7337814136','saitejashwini.h@perfios.com','Tejashwini'),('vunnati','vunnu7',2,'current',8400,'Atp','8478364744','vunnati@gmail.com','Vunnati'),('sami','sam',3,'savings',5500,'Kadapa','8243676573','sami@gmail.com','Sameer MD'),('balu','ball',4,'current',5000,'Andhra','8953746736','balu@gmail.com','Balaji V'),('tarun','tar',10,'current',5600,'Jodhpur','9849283723','tarun@gmail.com','Tarun'),('chiranth','chiru',12,'savings',5650,' Bangalore','9436187832','chiru@gmail.com','Chiranth'),('saurav','sau',13,'savings',5000,'Patna','9857834662','saurav@gmail.com','Saurav Suman'),('dilip','dil',14,'current',6350,'Blr','8437687637','dilip@gmail.com','Dilip G'),('arjit','ari',15,'current',4500,'blr','8874783642','arjit@gmail.com','Arijit Roy'),('asha','ash',16,'savings',5900,'Sindhanur','8473635975','ash@gmail.com','Asha'),('dev','dev',20,'current',4050,' blre','7215387469','dev@gmail.com','Devesh');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-10 16:44:10
